//
//  cycleView.h
//  NewApp
//
//  Created by Alan on 2019/4/10.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface cycleView : UIView
@property (assign)float progress;

- (void)drawProgress:(CGFloat )progress;

@end

NS_ASSUME_NONNULL_END
