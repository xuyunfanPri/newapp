//
//  cycleView.m
//  NewApp
//
//  Created by Alan on 2019/4/10.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "cycleView.h"

@implementation cycleView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    CGContextRef ctx = UIGraphicsGetCurrentContext();//获取上下文
    
    CGPoint center = CGPointMake(100, 100);  //设置圆心位置
    CGFloat radius = 90;  //设置半径
    CGFloat startA = - M_PI_2;  //圆起点位置
    CGFloat endA = -M_PI_2 + M_PI * 2 * _progress;  //圆终点位置
    
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startA endAngle:endA clockwise:YES];
    UIBezierPath *path2 = [UIBezierPath bezierPathWithArcCenter:center radius:45 startAngle:startA endAngle:endA clockwise:YES];
    
    CGContextSetLineWidth(ctx, 15); //设置线条宽度
    [[UIColor blueColor] setStroke]; //设置描边颜色
    
    CGContextAddPath(ctx, path.CGPath); //把路径添加到上下文
    CGContextAddPath(ctx, path2.CGPath);
    CGContextStrokePath(ctx);  //渲染

}
- (void)drawProgress:(CGFloat )progress
{
    _progress = progress;
    [self setNeedsDisplay];
}

@end
