//
//  XYProtocolViewController.m
//  NewApp
//
//  Created by Qhtu on 2019/5/4.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYUserCenterView.h"
#import "XYProtocolViewController.h"

@interface XYProtocolViewController ()

@property (nonatomic, strong) XYUserCenterProtocolView *protocolView;

@end

@implementation XYProtocolViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureTopView];
    [self configureUI];
}

- (void)configureTopView {
    [super configureTopView];
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    headerView.backgroundColor = [UIColor clearColor];
}

- (void)configureUI {
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    self.protocolView = [[XYUserCenterProtocolView alloc]init];
    [self.protocolView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight)];
    [self.view insertSubview:self.protocolView belowSubview:headerView];
 
}

@end
