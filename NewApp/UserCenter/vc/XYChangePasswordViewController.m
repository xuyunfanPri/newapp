//
//  XYChangePasswordViewController.m
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYUserCenterView.h"
#import "XYChangePasswordViewController.h"

#import "XYLoginViewController.h"

@interface XYChangePasswordViewController ()

@property (nonatomic, strong) XYUserCenterChangePasswordView *changePasswordView;

@end

@implementation XYChangePasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTopView];
    
    [self configureUI];
    
    // Do any additional setup after loading the view.
}

- (void)configureTopView {
    [super configureTopView];
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    headerView.backgroundColor = [UIColor clearColor];
}

- (void)configureUI {
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    self.changePasswordView = [[XYUserCenterChangePasswordView alloc]init];
    [self.changePasswordView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight)];
    [self.view insertSubview:self.changePasswordView belowSubview:headerView];
    
    [self.changePasswordView.changePasswordBtn addTarget:self action:@selector(changePassword:) forControlEvents:UIControlEventTouchUpInside];
    [self.changePasswordView.loginBtn addTarget:self action:@selector(goToLogin:) forControlEvents:UIControlEventTouchUpInside];
    
}

#pragma mark - CLICK EVENT

- (void)changePassword:(UIButton *)sender {
    
}

- (void)goToLogin:(UIButton *)sender {
    XYLoginViewController *loginVc = [[XYLoginViewController alloc]init];
    [self.navigationController pushViewController:loginVc animated:YES];
}

@end
