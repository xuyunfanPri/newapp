//
//  XYForgetPasswordViewController.m
//  NewApp
//
//  Created by Qhtu on 2019/4/30.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYUserCenterView.h"
#import "XYSelectRegionViewController.h"
#import "XYForgetPasswordViewController.h"

@interface XYForgetPasswordViewController ()

@property (nonatomic, strong) XYUserCenterForgetPasswordView *forgetView;

@end

@implementation XYForgetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTopView];
    
    [self configureUI];
    
    // Do any additional setup after loading the view.
}

- (void)configureTopView {
    [super configureTopView];
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    headerView.backgroundColor = [UIColor clearColor];
}

- (void)configureUI {
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    self.forgetView = [[XYUserCenterForgetPasswordView alloc]init];
    [self.forgetView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight)];
    [self.view insertSubview:self.forgetView belowSubview:headerView];
    
    [self.forgetView.loginBtn addTarget:self action:@selector(goSend:) forControlEvents:UIControlEventTouchUpInside];
    [self.forgetView.accountBtn addTarget:self action:@selector(goToLogin:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)goSend:(UIButton *)sender {
    
}

- (void)goToLogin:(UIButton *)sender {
    XYSelectRegionViewController *vc = [[XYSelectRegionViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
