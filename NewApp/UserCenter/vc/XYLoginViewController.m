//
//  XYLoginViewController.m
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYUserCenterView.h"
#import "XYForgetPasswordViewController.h"
#import "XYClaimAccountViewController.h"
#import "XYLoginViewController.h"

@interface XYLoginViewController ()

@property (nonatomic, strong) XYUserCenterLoginView *loginView;

@end

@implementation XYLoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTopView];
 
    [self configureUI];
    
    // Do any additional setup after loading the view.
}

- (void)configureTopView {
    [super configureTopView];
//    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
//    headerView.backgroundColor = [UIColor clearColor];
}

- (void)configureUI {
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    self.loginView = [[XYUserCenterLoginView alloc]init];
    [self.loginView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight)];
    [self.view insertSubview:self.loginView belowSubview:headerView];
    
    [self.loginView.loginBtn addTarget:self action:@selector(goLogin:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.accountBtn addTarget:self action:@selector(goToClaimAccount:) forControlEvents:UIControlEventTouchUpInside];
    [self.loginView.forgetBtn addTarget:self action:@selector(goToForgetPassword:) forControlEvents:UIControlEventTouchUpInside];
}

#pragma mark - CLICK EVENT

/**
 登陆
 @param sender
 */
- (void)goLogin:(UIButton *)sender {
    //请求登陆
    
}

- (void)goToClaimAccount:(UIButton *)sender {
    //跳转绑定账号
    XYClaimAccountViewController *accountVc = [[XYClaimAccountViewController alloc]init];
    [self.navigationController pushViewController:accountVc animated:YES];
}

- (void)goToForgetPassword:(UIButton *)sender {
    XYForgetPasswordViewController *forgetVc = [[XYForgetPasswordViewController alloc]init];
    [self.navigationController pushViewController:forgetVc animated:YES];
}

@end
