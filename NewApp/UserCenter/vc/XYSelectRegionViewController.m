//
//  XYSelectRegionViewController.m
//  NewApp
//
//  Created by Qhtu on 2019/4/30.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "NewPagedFlowView.h"
#import "PGIndexNormalBanner.h"
#import "XYSelectRegionViewController.h"

@interface XYSelectRegionViewController ()<NewPagedFlowViewDelegate,NewPagedFlowViewDataSource>

@property (nonatomic, strong) UILabel *topLab1;
@property (nonatomic, strong) UILabel *topLab2;
@property (nonatomic, strong) NewPagedFlowView *flowView;
@property (nonatomic, strong) UIButton *confirmBtn;

@end

@implementation XYSelectRegionViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTopView];
    
    [self configureUI];
    // Do any additional setup after loading the view.
}

- (void)configureUI {
    _topLab1 = [[UILabel alloc]initWithFrame:CGRectMake(PIXEL_FITS(10), PIXEL_FITS(80), SCREEN_WIDTH - PIXEL_FITS(20), PIXEL_FITS(25))];
    _topLab1.textAlignment = NSTextAlignmentCenter;
    _topLab1.textColor = [UIColor whiteColor];
    _topLab1.font = [UIFont boldSystemFontOfSize:25];
    _topLab1.text = @"SELECT";
    [self.view addSubview:self.topLab1];
    
    _topLab2 = [[UILabel alloc]initWithFrame:CGRectMake(PIXEL_FITS(10), CGRectGetMaxY(_topLab1.frame) + PIXEL_FITS(15), SCREEN_WIDTH - PIXEL_FITS(20), PIXEL_FITS(25))];
    _topLab2.textAlignment = NSTextAlignmentCenter;
    _topLab2.text = @"REGION";
    _topLab2.textColor = [UIColor whiteColor];
    _topLab2.font = [UIFont boldSystemFontOfSize:25];
    [self.view addSubview:self.topLab2];
    
    CGFloat flowHeight = floor(SCREEN_WIDTH * (36.0 / 47.0));
    _flowView = [[NewPagedFlowView alloc] initWithFrame:CGRectMake(0, CGRectGetMaxY(_topLab2.frame), SCREEN_WIDTH, flowHeight)];
    _flowView.backgroundColor = [UIColor clearColor];
    _flowView.delegate = self;
    _flowView.dataSource = self;
    _flowView.minimumPageAlpha = 0.1;
    _flowView.orientation = NewPagedFlowViewOrientationHorizontal;
    _flowView.leftRightMargin = 70.0;
    _flowView.topBottomMargin = 50.0;
    _flowView.isOpenAutoScroll = NO;
    _flowView.isCarousel = NO;
    [self.view addSubview:self.flowView];
    
    self.confirmBtn = [[UIButton alloc]initWithFrame:CGRectMake(PIXEL_FITS(50), CGRectGetMaxY(self.flowView.frame) + PIXEL_FITS(50), SCREEN_WIDTH - PIXEL_FITS(100), PIXEL_FITS(50))];
    self.confirmBtn.backgroundColor = [UIColor clearColor];
    [self.confirmBtn.layer setMasksToBounds:YES];
    [self.confirmBtn setTitle:@"CONFIRM" forState:UIControlStateNormal];
    [self.confirmBtn setTitleColor:[UIColor getHegColor:@"e5ce10"] forState:UIControlStateNormal];
    [self.confirmBtn.layer setBorderColor:[UIColor getHegColor:@"e5ce10" WithAlpha:1].CGColor];
    [self.confirmBtn.layer setBorderWidth:1];
    [self.confirmBtn.layer setCornerRadius:3];
    [self.view addSubview:self.confirmBtn];
    
    [self.flowView reloadData];
}

#pragma mark - NewPagedFlowViewDelegate

- (NSInteger)numberOfPagesInFlowView:(NewPagedFlowView *)flowView {
    return 5;
}

- (UIView *)flowView:(NewPagedFlowView *)flowView cellForPageAtIndex:(NSInteger)index {
    PGIndexNormalBanner *bannerView = (PGIndexNormalBanner *)[flowView dequeueReusableCell];
    if (!bannerView) {
        bannerView = [[PGIndexNormalBanner alloc]initWithFrame:CGRectZero];
        bannerView.tag = index;
    }
//    JSBCBaseModel *baseModel = self.viewModel.newsBannerDatas [index];
//    if ([JSBCSettingModel sharedInstance].isNoImageMode) {
//        [bannerView.mainImageView setImage:TTIMAGE_PLACE_HOLDER];
//    } else {
//        [bannerView.mainImageView sd_setImageWithURL:[NSURL URLWithString:baseModel.otherPhoto?:baseModel.photo] placeholderImage:TTIMAGE_PLACE_HOLDER];
//    }
    bannerView.mainImageView.backgroundColor = [UIColor lightGrayColor];
    [bannerView.titleLabel setText:@"AUSTRALIA"];
    return bannerView;
}

- (CGSize)sizeForPageInFlowView:(NewPagedFlowView *)flowView {
//    JSBCBaseModel *baseModel = [self.viewModel.newsBannerDatas firstObject];
    //
    CGFloat flowWidth;
    flowWidth = SCREEN_WIDTH - 140.0;
    
    CGFloat flowHeight = floor(flowWidth * (36.0 / 47.0));
    return CGSizeMake(flowWidth, flowHeight);
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
