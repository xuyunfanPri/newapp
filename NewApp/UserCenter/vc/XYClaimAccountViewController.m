//
//  XYClaimAccountViewController.m
//  NewApp
//
//  Created by Qhtu on 2019/4/30.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYUserCenterView.h"
#import "XYProtocolViewController.h"
#import "XYClaimAccountViewController.h"

@interface XYClaimAccountViewController ()

@property (nonatomic, strong) XYUserCenterClaimAccountView *accountView;

@end

@implementation XYClaimAccountViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self configureTopView];
    
    [self configureUI];
    
    // Do any additional setup after loading the view.
}

- (void)configureTopView {
    [super configureTopView];
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    headerView.backgroundColor = [UIColor clearColor];
}

- (void)configureUI {
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    self.accountView = [[XYUserCenterClaimAccountView alloc]init];
    [self.accountView setFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT - kTabBarHeight)];
    [self.view insertSubview:self.accountView belowSubview:headerView];
    
    [self.accountView.loginBtn addTarget:self action:@selector(goLogin:) forControlEvents:UIControlEventTouchUpInside];
    [self.accountView.accountBtn addTarget:self action:@selector(goToClaimAccount:) forControlEvents:UIControlEventTouchUpInside];
    [self.accountView.delegateView.overBtn addTarget:self action:@selector(goToProtocol:) forControlEvents:UIControlEventTouchUpInside];
    
}

- (void)goLogin:(UIButton *)sender {
    
}

- (void)goToClaimAccount:(UIButton *)sender {
    
}

- (void)goToProtocol:(UIButton *)sender {
    XYProtocolViewController *vc = [[XYProtocolViewController alloc]init];
    [self.navigationController pushViewController:vc animated:YES];
}

@end
