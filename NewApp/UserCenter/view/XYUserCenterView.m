//
//  XYUserCenterView.m
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//

#import "XYUserCenterView.h"

@implementation XYCustomTextField

- (instancetype)init {
    if (self = [super init]) {
        self.placeHolder = [[UILabel alloc]init];
        self.placeHolder.textAlignment = NSTextAlignmentCenter;
        self.placeHolder.backgroundColor = [UIColor clearColor];
        [self addSubview:self.placeHolder];
        
        self.textField = [[UITextField alloc]init];
        self.textField.backgroundColor = [UIColor clearColor];
        self.textField.font = [UIFont systemFontOfSize:18];
        self.textField.textColor = [UIColor whiteColor];
        self.textField.delegate = self;
        [self.textField addTarget:self action:@selector(changeTextField:) forControlEvents:UIControlEventEditingChanged];
        [self addSubview:self.textField];
        
        [self.placeHolder mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.right.top.equalTo(self);
        }];
        
        [self.textField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.bottom.top.equalTo(self);
            make.left.equalTo(self.mas_left).offset(15);
            make.right.equalTo(self.mas_right).offset(-15);
        }];
    }
    return self;
}

#pragma mark - UITextFieldDelegate

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    
}

- (void)changeTextField:(UITextField *)sender {
    if (_textField.text.length) {
        self.placeHolder.hidden = YES;
    } else {
        self.placeHolder.hidden = NO;
    }
}

@end

@implementation XYUserDelegateView

- (instancetype)init {
    if (self = [super init]) {
        self.clickBoxBtn = [[UIButton alloc]init];
        self.clickBoxBtn.backgroundColor = [UIColor clearColor];
        [self.clickBoxBtn.layer setMasksToBounds:YES];
        [self.clickBoxBtn.layer setCornerRadius:3];
        [self.clickBoxBtn.layer setBorderColor:[UIColor whiteColor].CGColor];
        [self.clickBoxBtn.layer setBorderWidth:1];
        [self.clickBoxBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
        [self.clickBoxBtn addTarget:self action:@selector(actClickBoxBtn:) forControlEvents:UIControlEventTouchUpInside];
        [self addSubview:self.clickBoxBtn];
        
        self.delegateLab = [[UILabel alloc]init];
        self.delegateLab.font = [UIFont systemFontOfSize:16];
        self.delegateLab.text = @"I agree to the scopesuite privacy policy";
        self.delegateLab.textColor = [UIColor whiteColor];
        [self addSubview:self.delegateLab];
        
        self.overBtn = [[UIButton alloc]init];
        [self.overBtn setBackgroundColor:[UIColor clearColor]];
        [self addSubview:self.overBtn];
        
        [self.clickBoxBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.mas_top).offset(10);
            make.left.equalTo(self);
            make.bottom.equalTo(self.mas_bottom).offset(-10);
            make.width.equalTo(self.clickBoxBtn.mas_height);
        }];
        
        [self.delegateLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.clickBoxBtn.mas_right).offset(10);
            make.right.equalTo(self.mas_right);
            make.centerY.equalTo(self.mas_centerY);
            make.top.greaterThanOrEqualTo(self.mas_top);
        }];
        
        [self.overBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.bottom.top.equalTo(self.delegateLab);
        }];
    }
    return self;
}

- (void)actClickBoxBtn:(UIButton *)sender {
    if (sender.selected) {
        [self.clickBoxBtn setImage:[UIImage imageNamed:@""] forState:UIControlStateNormal];
    } else {
        [self.clickBoxBtn setImage:[UIImage imageNamed:@"btn_selected"] forState:UIControlStateNormal];
    }
    
    self.clickBoxBtn.selected = !self.clickBoxBtn.selected;
}

@end

@interface XYUserCenterTopView()

@property (nonatomic, readwrite) UIImageView *logoImg;
@property (nonatomic, readwrite) UILabel *topLab1;
@property (nonatomic, readwrite) UILabel *topLab2;

@end

@implementation XYUserCenterTopView

- (instancetype)init {
    if (self = [super init]) {
        
        self.logoImg = [[UIImageView alloc]init];
        self.logoImg.backgroundColor = [UIColor clearColor];
        [self.logoImg setImage:[UIImage imageNamed:@""]];
        [self addSubview:self.logoImg];
        
        self.topLab1 = [[UILabel alloc]init];
        self.topLab1.textAlignment = NSTextAlignmentCenter;
        self.topLab1.font = [UIFont boldSystemFontOfSize:30];
        self.topLab1.textColor = [UIColor whiteColor];
        [self addSubview:self.topLab1];
        
        self.topLab2 = [[UILabel alloc]init];
        self.topLab2.textAlignment = NSTextAlignmentCenter;
        self.topLab2.font = [UIFont boldSystemFontOfSize:30];
        self.topLab2.textColor = [UIColor whiteColor];
        [self addSubview:self.topLab2];
        
        [self.logoImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.mas_top).offset(PIXEL_FITS(50));
            make.left.mas_lessThanOrEqualTo(self.mas_left).offset(10);
            make.width.equalTo(@150);
            make.height.equalTo(@25);
        }];
        
        [self.topLab1 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.logoImg.mas_bottom).offset(PIXEL_FITS(25));
            make.left.mas_lessThanOrEqualTo(self.mas_left).offset(10);
        }];
        
        [self.topLab2 mas_makeConstraints:^(MASConstraintMaker *make) {
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.topLab1.mas_bottom);
            make.left.mas_lessThanOrEqualTo(self.mas_left).offset(10);
            make.bottom.equalTo(self.mas_bottom).offset(-PIXEL_FITS(30));
        }];
        
    }
    return self;
}

@end

@interface XYUserCenterLoginView()

@property (nonatomic, readwrite) XYUserCenterTopView *topView;
@property (nonatomic, readwrite) XYCustomTextField *userNameTextField;
@property (nonatomic, readwrite) XYCustomTextField *passwordTextField;
@property (nonatomic, readwrite) UIButton *accountBtn;
@property (nonatomic, readwrite) UIButton *loginBtn;
@property (nonatomic, readwrite) UIButton *forgetBtn;
@property (nonatomic, readwrite) UIImageView *bgImg;

@end

@implementation XYUserCenterLoginView

- (instancetype)init {
    if (self = [super init]) {
        
        self.bgImg = [[UIImageView alloc]init];
        self.bgImg.backgroundColor = [UIColor clearColor];
        [self.bgImg setImage:[UIImage imageNamed:@"bg.png"]];
        [self addSubview:self.bgImg];
        
        self.topView = [[XYUserCenterTopView alloc]init];
        self.topView.backgroundColor = [UIColor clearColor];
        self.topView.topLab1.text = @"MEMBER";
        self.topView.topLab2.text = @"LOGIN";
        
        [self addSubview:self.topView];
        
        self.userNameTextField = [[XYCustomTextField alloc]init];
        self.userNameTextField.backgroundColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.15];
        self.userNameTextField.placeHolder.text = @"Confirm New Password";
        self.userNameTextField.placeHolder.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.userNameTextField.placeHolder.font = [UIFont boldSystemFontOfSize:18];
        [self addSubview:self.userNameTextField];
        
        self.passwordTextField = [[XYCustomTextField alloc]init];
        self.passwordTextField.backgroundColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.15];
        self.passwordTextField.placeHolder.text = @"New Password";
        self.passwordTextField.placeHolder.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.passwordTextField.placeHolder.font = [UIFont boldSystemFontOfSize:18];
        [self addSubview:self.passwordTextField];
        
        self.loginBtn = [[UIButton alloc]init];
        self.loginBtn.backgroundColor = [UIColor clearColor];
        [self.loginBtn.layer setMasksToBounds:YES];
        [self.loginBtn.layer setBorderColor:[UIColor getHegColor:@"e5ce10" WithAlpha:1].CGColor];
        [self.loginBtn.layer setBorderWidth:1];
        [self.loginBtn.layer setCornerRadius:3];
        [self.loginBtn setTitle:@"LOGIN" forState:UIControlStateNormal];
        [self.loginBtn setTitleColor:[UIColor getHegColor:@"e5ce10"] forState:UIControlStateNormal];
        [self addSubview:self.loginBtn];
        
        self.accountBtn = [[UIButton alloc]init];
        self.accountBtn.backgroundColor = [UIColor clearColor];
        [self.accountBtn.layer setMasksToBounds:YES];
        [self.accountBtn setTitle:@"CLAIM ACCOUNT" forState:UIControlStateNormal];
        [self.accountBtn setTitleColor:[UIColor getHegColor:@"008be4"] forState:UIControlStateNormal];
        [self.accountBtn.layer setBorderColor:[UIColor getHegColor:@"008be4" WithAlpha:1].CGColor];
        [self.accountBtn.layer setBorderWidth:1];
        [self.accountBtn.layer setCornerRadius:3];
        [self addSubview:self.accountBtn];
        
        self.forgetBtn = [[UIButton alloc]init];
        self.forgetBtn.backgroundColor = [UIColor clearColor];
        [self.forgetBtn setTitle:@"Forget Your Password?" forState:UIControlStateNormal];
        [self.forgetBtn setTitleColor:[UIColor getHegColor:@"ffffff" WithAlpha:0.3] forState:UIControlStateNormal];
        [self addSubview:self.forgetBtn];
        
        [self.bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.top.right.equalTo(self);
        }];
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self.mas_top).offset(PIXEL_FITS(50));
        }];
        
        [self.userNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.topView.mas_bottom);
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.userNameTextField.mas_bottom).offset(PIXEL_FITS(20));
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.passwordTextField.mas_bottom).offset(PIXEL_FITS(20));
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.accountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.loginBtn.mas_bottom).offset(PIXEL_FITS(25));
            make.height.equalTo(@(PIXEL_FITS(50)));
            
        }];
        
        [self.forgetBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.greaterThanOrEqualTo(self.mas_left).offset(PIXEL_FITS(10));
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.accountBtn.mas_bottom).offset(PIXEL_FITS(25));
            make.bottom.lessThanOrEqualTo(self.mas_bottom);
        }];
        
    }
    return self;
}

@end

@interface XYUserCenterChangePasswordView()

@property (nonatomic, readwrite) XYUserCenterTopView *topView;
@property (nonatomic, readwrite) XYCustomTextField *surePasswordTextField;
@property (nonatomic, readwrite) XYCustomTextField *passwordTextField;
@property (nonatomic, readwrite) UIButton *changePasswordBtn;
@property (nonatomic, readwrite) UIButton *loginBtn;
@property (nonatomic, readwrite) UIImageView *bgImg;

@end

@implementation XYUserCenterChangePasswordView

- (instancetype)init {
    if (self = [super init]) {

        self.bgImg = [[UIImageView alloc]init];
        self.bgImg.backgroundColor = [UIColor clearColor];
        [self.bgImg setImage:[UIImage imageNamed:@"bg.png"]];
        [self addSubview:self.bgImg];
        
        self.topView = [[XYUserCenterTopView alloc]init];
        self.topView.backgroundColor = [UIColor clearColor];
        self.topView.topLab1.text = @"CHANGE";
        self.topView.topLab2.text = @"PASSWORD";
        
        [self addSubview:self.topView];
        
        self.passwordTextField = [[XYCustomTextField alloc]init];
        self.passwordTextField.backgroundColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.15];
        self.passwordTextField.placeHolder.text = @"New Password";
        self.passwordTextField.placeHolder.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.passwordTextField.placeHolder.font = [UIFont boldSystemFontOfSize:18];
        [self addSubview:self.passwordTextField];
        
        self.surePasswordTextField = [[XYCustomTextField alloc]init];
        self.surePasswordTextField.backgroundColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.15];
        self.surePasswordTextField.placeHolder.text = @"Confirm New Password";
        self.surePasswordTextField.placeHolder.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.surePasswordTextField.placeHolder.font = [UIFont boldSystemFontOfSize:18];
        [self addSubview:self.surePasswordTextField];
        
        self.changePasswordBtn = [[UIButton alloc]init];
        self.changePasswordBtn.backgroundColor = [UIColor clearColor];
        [self.changePasswordBtn.layer setMasksToBounds:YES];
        [self.changePasswordBtn.layer setBorderColor:[UIColor getHegColor:@"e5ce10" WithAlpha:1].CGColor];
        [self.changePasswordBtn.layer setBorderWidth:1];
        [self.changePasswordBtn.layer setCornerRadius:3];
        [self.changePasswordBtn setTitle:@"CHANGE PASSWORD" forState:UIControlStateNormal];
        [self.changePasswordBtn setTitleColor:[UIColor getHegColor:@"e5ce10"] forState:UIControlStateNormal];
        [self addSubview:self.changePasswordBtn];
        
        self.loginBtn = [[UIButton alloc]init];
        self.loginBtn.backgroundColor = [UIColor clearColor];
        [self.loginBtn.layer setMasksToBounds:YES];
        [self.loginBtn setTitle:@"LOGIN" forState:UIControlStateNormal];
        [self.loginBtn.layer setBorderColor:[UIColor getHegColor:@"008be4" WithAlpha:1].CGColor];
        [self.loginBtn.layer setBorderWidth:1];
        [self.loginBtn.layer setCornerRadius:3];
        [self.loginBtn setTitleColor:[UIColor getHegColor:@"008be4"] forState:UIControlStateNormal];
        [self addSubview:self.loginBtn];
        
        [self.bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.top.right.equalTo(self);
        }];
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self.mas_top).offset(PIXEL_FITS(50));
        }];
        
        [self.passwordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.topView.mas_bottom);
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.surePasswordTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.passwordTextField.mas_bottom).offset(PIXEL_FITS(20));
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.changePasswordBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.surePasswordTextField.mas_bottom).offset(PIXEL_FITS(20));
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.changePasswordBtn.mas_bottom).offset(PIXEL_FITS(25));
            make.height.equalTo(@(PIXEL_FITS(50)));
            make.bottom.lessThanOrEqualTo(self.mas_bottom);
        }];
        
    }
    return self;
}

@end

@interface XYUserCenterClaimAccountView ()

@property (nonatomic, readwrite) UIImageView *bgImg;
@property (nonatomic, readwrite) XYUserCenterTopView *topView;
@property (nonatomic, readwrite) XYCustomTextField *userNameTextField;
@property (nonatomic, readwrite) XYUserDelegateView *delegateView;
@property (nonatomic, readwrite) UIButton *accountBtn;
@property (nonatomic, readwrite) UIButton *loginBtn;
@property (nonatomic, readwrite) UILabel *bottomTipLab;

@end

@implementation XYUserCenterClaimAccountView

- (instancetype)init {
    if (self = [super init]) {
        self.bgImg = [[UIImageView alloc]init];
        self.bgImg.backgroundColor = [UIColor clearColor];
        [self.bgImg setImage:[UIImage imageNamed:@"bg.png"]];
        [self addSubview:self.bgImg];
        
        self.topView = [[XYUserCenterTopView alloc]init];
        self.topView.backgroundColor = [UIColor clearColor];
        self.topView.topLab1.text = @"CLAIM YOUR";
        self.topView.topLab2.text = @"ACCOUNT";
        
        [self addSubview:self.topView];
        
        self.userNameTextField = [[XYCustomTextField alloc]init];
        self.userNameTextField.backgroundColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.15];
        self.userNameTextField.placeHolder.text = @"Username";
        self.userNameTextField.placeHolder.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.userNameTextField.placeHolder.font = [UIFont boldSystemFontOfSize:18];
        [self addSubview:self.userNameTextField];
        
        self.delegateView = [[XYUserDelegateView alloc]init];
        self.delegateView.backgroundColor = [UIColor clearColor];
        [self addSubview:self.delegateView];
        
        self.accountBtn = [[UIButton alloc]init];
        self.accountBtn.backgroundColor = [UIColor clearColor];
        [self.accountBtn.layer setMasksToBounds:YES];
        [self.accountBtn.layer setBorderColor:[UIColor getHegColor:@"e5ce10" WithAlpha:1].CGColor];
        [self.accountBtn.layer setBorderWidth:1];
        [self.accountBtn.layer setCornerRadius:3];
        [self.accountBtn setTitle:@"CLAIM ACCOUNT" forState:UIControlStateNormal];
        [self.accountBtn setTitleColor:[UIColor getHegColor:@"e5ce10"] forState:UIControlStateNormal];
        [self addSubview:self.accountBtn];
        
        self.loginBtn = [[UIButton alloc]init];
        self.loginBtn.backgroundColor = [UIColor clearColor];
        [self.loginBtn.layer setMasksToBounds:YES];
        [self.loginBtn setTitle:@"LOGIN" forState:UIControlStateNormal];
        [self.loginBtn.layer setBorderColor:[UIColor getHegColor:@"008be4" WithAlpha:1].CGColor];
        [self.loginBtn.layer setBorderWidth:1];
        [self.loginBtn.layer setCornerRadius:3];
        [self.loginBtn setTitleColor:[UIColor getHegColor:@"008be4"] forState:UIControlStateNormal];
        [self addSubview:self.loginBtn];
        
        self.bottomTipLab = [[UILabel alloc]init];
        self.bottomTipLab.numberOfLines = 0;
        self.bottomTipLab.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.bottomTipLab.font = [UIFont systemFontOfSize:15];
        self.bottomTipLab.hidden = YES;
//        self.bottomTipLab.text = @"If you have any questions or concerns about claiming your account,please do not hesitate to contact us at helpdesk@scopesuite.com au or 0282491880";
        [self addSubview:self.bottomTipLab];
        
        [self.bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.top.right.equalTo(self);
        }];
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self.mas_top).offset(PIXEL_FITS(50));
        }];
        
        [self.userNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.topView.mas_bottom);
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.delegateView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.userNameTextField.mas_bottom).offset(PIXEL_FITS(10));
            make.left.greaterThanOrEqualTo(self.mas_left);
            make.centerX.equalTo(self.mas_centerX);
            make.height.equalTo(@40);
        }];
        
        [self.accountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.delegateView.mas_bottom).offset(PIXEL_FITS(20));
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.accountBtn.mas_bottom).offset(PIXEL_FITS(20));
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.bottomTipLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.loginBtn.mas_bottom).offset(PIXEL_FITS(30));
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.bottom.lessThanOrEqualTo(self.mas_bottom);
        }];
    }
    return self;
}

@end

@interface XYUserCenterForgetPasswordView()

@property (nonatomic, readwrite) UIImageView *bgImg;
@property (nonatomic, readwrite) XYUserCenterTopView *topView;
@property (nonatomic, readwrite) XYCustomTextField *userNameTextField;
@property (nonatomic, readwrite) UILabel *tipLab;
@property (nonatomic, readwrite) UIButton *accountBtn;
@property (nonatomic, readwrite) UIButton *loginBtn;
@property (nonatomic, readwrite) UILabel *bottomTipLab;

@end

@implementation XYUserCenterForgetPasswordView

- (instancetype)init {
    if (self = [super init]) {
        self.bgImg = [[UIImageView alloc]init];
        self.bgImg.backgroundColor = [UIColor clearColor];
        [self.bgImg setImage:[UIImage imageNamed:@"bg.png"]];
        [self addSubview:self.bgImg];
        
        self.topView = [[XYUserCenterTopView alloc]init];
        self.topView.backgroundColor = [UIColor clearColor];
        self.topView.topLab1.text = @"CLAIM YOUR";
        self.topView.topLab2.text = @"ACCOUNT";
        
        [self addSubview:self.topView];
        
        self.userNameTextField = [[XYCustomTextField alloc]init];
        self.userNameTextField.backgroundColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.15];
        self.userNameTextField.placeHolder.text = @"Username";
        self.userNameTextField.placeHolder.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.userNameTextField.placeHolder.font = [UIFont boldSystemFontOfSize:18];
        [self addSubview:self.userNameTextField];
        
        self.tipLab = [[UILabel alloc]init];
        self.tipLab.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.tipLab.numberOfLines = 0;
        self.tipLab.font = [UIFont systemFontOfSize:15];
        self.tipLab.text = @"Please enter your username to start password recovery";
        self.tipLab.backgroundColor = [UIColor clearColor];
        self.tipLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.tipLab];
        
        self.accountBtn = [[UIButton alloc]init];
        self.accountBtn.backgroundColor = [UIColor clearColor];
        [self.accountBtn.layer setMasksToBounds:YES];
        [self.accountBtn.layer setBorderColor:[UIColor getHegColor:@"e5ce10" WithAlpha:1].CGColor];
        [self.accountBtn.layer setBorderWidth:1];
        [self.accountBtn.layer setCornerRadius:3];
        [self.accountBtn setTitle:@"SEND" forState:UIControlStateNormal];
        [self.accountBtn setTitleColor:[UIColor getHegColor:@"e5ce10"] forState:UIControlStateNormal];
        [self addSubview:self.accountBtn];
        
        self.loginBtn = [[UIButton alloc]init];
        self.loginBtn.backgroundColor = [UIColor clearColor];
        [self.loginBtn.layer setMasksToBounds:YES];
        [self.loginBtn setTitle:@"LOGIN" forState:UIControlStateNormal];
        [self.loginBtn.layer setBorderColor:[UIColor getHegColor:@"008be4" WithAlpha:1].CGColor];
        [self.loginBtn.layer setBorderWidth:1];
        [self.loginBtn.layer setCornerRadius:3];
        [self.loginBtn setTitleColor:[UIColor getHegColor:@"008be4"] forState:UIControlStateNormal];
        [self addSubview:self.loginBtn];
        
        self.bottomTipLab = [[UILabel alloc]init];
        self.bottomTipLab.numberOfLines = 0;
        self.bottomTipLab.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.bottomTipLab.font = [UIFont systemFontOfSize:15];
        self.bottomTipLab.text = @"If you have any questions or concerns about claiming your account,please do not hesitate to contact us at helpdesk@scopesuite.com au or 0282491880";
        self.bottomTipLab.textAlignment = NSTextAlignmentCenter;
        [self addSubview:self.bottomTipLab];
        
        [self.bgImg mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.bottom.top.right.equalTo(self);
        }];
        
        [self.topView mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.right.equalTo(self);
            make.top.equalTo(self.mas_top).offset(PIXEL_FITS(50));
        }];
        
        [self.userNameTextField mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.topView.mas_bottom);
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.tipLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.userNameTextField.mas_bottom).offset(PIXEL_FITS(15));
            make.left.greaterThanOrEqualTo(self.mas_left).offset(PIXEL_FITS(50));
            make.centerX.equalTo(self.mas_centerX);
        }];
        
        [self.accountBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.tipLab.mas_bottom).offset(PIXEL_FITS(40));
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.loginBtn mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.top.equalTo(self.accountBtn.mas_bottom).offset(PIXEL_FITS(20));
            make.height.equalTo(@(PIXEL_FITS(50)));
        }];
        
        [self.bottomTipLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(self.loginBtn.mas_bottom).offset(PIXEL_FITS(30));
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.right.equalTo(self.mas_right).offset(-PIXEL_FITS(50));
            make.bottom.lessThanOrEqualTo(self.mas_bottom);
        }];
    }
    return self;
}

@end

@interface XYUserCenterProtocolView()

@property (nonatomic, readwrite) UILabel *titleLab;
@property (nonatomic, readwrite) UILabel *subTitleLab;
@property (nonatomic, readwrite) UILabel *contentLab;

@end

@implementation XYUserCenterProtocolView

- (instancetype)init {
    if (self = [super init]) {
        self.titleLab = [[UILabel alloc]init];
        self.titleLab.textAlignment = NSTextAlignmentCenter;
        self.titleLab.textColor = [UIColor whiteColor];
        self.titleLab.font = [UIFont boldSystemFontOfSize:30];
        self.titleLab.text = @"PRICACY POLICY";
        [self addSubview:self.titleLab];
        
        self.subTitleLab = [[UILabel alloc]init];
        self.subTitleLab.textAlignment = NSTextAlignmentCenter;
        self.subTitleLab.textColor = [UIColor whiteColor];
        self.subTitleLab.font = [UIFont systemFontOfSize:20];
        self.subTitleLab.text = @"SCOPESUITE LEARNER APP";
        [self addSubview:self.subTitleLab];
        
        self.contentLab = [[UILabel alloc]init];
        self.contentLab.textAlignment = NSTextAlignmentCenter;
        self.contentLab.numberOfLines = 0;
        self.contentLab.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.3];
        self.contentLab.font = [UIFont systemFontOfSize:18];
        self.contentLab.text = @"Last a document unanimously agreed upon. Last a document unanimously agreed upon. Last a document unanimously agreed upon. Last a document unanimously agreed upon. \n\n  Last a document unanimously agreed upon. Last a document unanimously agreed upon.Last a document unanimously agreed upon. \n\n Last a document unanimously agreed upon. Last a document unanimously agreed upon.";
        [self addSubview:self.contentLab];
        
        [self.titleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.mas_top).offset(PIXEL_FITS(65));
        }];
        
        [self.subTitleLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(70));
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.titleLab.mas_bottom).offset(PIXEL_FITS(30));
        }];
        
        [self.contentLab mas_makeConstraints:^(MASConstraintMaker *make) {
            make.left.equalTo(self.mas_left).offset(PIXEL_FITS(50));
            make.centerX.equalTo(self.mas_centerX);
            make.top.equalTo(self.subTitleLab.mas_bottom).offset(PIXEL_FITS(45));
            make.bottom.lessThanOrEqualTo(self.mas_bottom).offset(PIXEL_FITS(50));
        }];
    }
    return self;
}


@end
