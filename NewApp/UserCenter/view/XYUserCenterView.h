//
//  XYUserCenterView.h
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYBaseView.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@interface XYCustomTextField : UIView <UITextFieldDelegate>

@property (nonatomic, strong) UITextField  *textField;
@property (nonatomic, strong) UILabel     *placeHolder;

@end


/**
 用户协议
 */
@interface XYUserDelegateView : UIView

@property (nonatomic, strong) UIButton    *clickBoxBtn;
@property (nonatomic, strong) UILabel     *delegateLab;
@property (nonatomic, strong) UIButton    *overBtn;

@end

@interface XYUserCenterTopView : XYBaseView

@property (nonatomic, readonly) UIImageView *logoImg;
@property (nonatomic, readonly) UILabel *topLab1;
@property (nonatomic, readonly) UILabel *topLab2;

@end

@interface XYUserCenterLoginView : XYBaseView

@property (nonatomic, readonly) UIImageView *bgImg;
@property (nonatomic, readonly) XYUserCenterTopView *topView;
@property (nonatomic, readonly) XYCustomTextField *userNameTextField;
@property (nonatomic, readonly) XYCustomTextField *passwordTextField;
@property (nonatomic, readonly) UIButton *accountBtn;
@property (nonatomic, readonly) UIButton *loginBtn;
@property (nonatomic, readonly) UIButton *forgetBtn;

@end

@interface XYUserCenterChangePasswordView : XYBaseView

@property (nonatomic, readonly) UIImageView *bgImg;
@property (nonatomic, readonly) XYUserCenterTopView *topView;
@property (nonatomic, readonly) XYCustomTextField *surePasswordTextField;
@property (nonatomic, readonly) XYCustomTextField *passwordTextField;
@property (nonatomic, readonly) UIButton *changePasswordBtn;
@property (nonatomic, readonly) UIButton *loginBtn;

@end

@interface XYUserCenterClaimAccountView : XYBaseView

@property (nonatomic, readonly) UIImageView *bgImg;
@property (nonatomic, readonly) XYUserCenterTopView *topView;
@property (nonatomic, readonly) XYCustomTextField *userNameTextField;
@property (nonatomic, readonly) XYUserDelegateView *delegateView;
@property (nonatomic, readonly) UIButton *accountBtn;
@property (nonatomic, readonly) UIButton *loginBtn;
@property (nonatomic, readonly) UILabel *bottomTipLab;

@end

@interface XYUserCenterForgetPasswordView : XYBaseView

@property (nonatomic, readonly) UIImageView *bgImg;
@property (nonatomic, readonly) XYUserCenterTopView *topView;
@property (nonatomic, readonly) XYCustomTextField *userNameTextField;
@property (nonatomic, readonly) UILabel *tipLab;
@property (nonatomic, readonly) UIButton *accountBtn;
@property (nonatomic, readonly) UIButton *loginBtn;
@property (nonatomic, readonly) UILabel *bottomTipLab;

@end

@interface XYUserCenterProtocolView : XYBaseView

@property (nonatomic, readonly) UILabel *titleLab;
@property (nonatomic, readonly) UILabel *subTitleLab;
@property (nonatomic, readonly) UILabel *contentLab;

@end

NS_ASSUME_NONNULL_END
