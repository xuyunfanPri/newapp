//
//  AppDelegate.h
//  NewApp
//
//  Created by Alan on 2019/4/10.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

