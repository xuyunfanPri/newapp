//
//  ViewUtils.m
//  NewApp
//
//  Created by Alan on 2019/4/25.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "ViewUtils.h"

@implementation ViewUtils

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/
+(CGFloat)getSpaceLabelHeightwithContentString:(NSString *)content andFont:(float)font andViewWidth:(float)viewWidth
{
    NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc] init];
    paraStyle.lineSpacing = 4.0f;
    NSDictionary *dic = @{NSFontAttributeName:[UIFont systemFontOfSize:font], NSParagraphStyleAttributeName:paraStyle};
    float linePadding = 16;
    CGSize size = [content boundingRectWithSize:CGSizeMake(viewWidth-15 - linePadding,MAXFLOAT) options:NSStringDrawingUsesLineFragmentOrigin attributes:dic context:nil].size;
    return size.height;
}
@end
