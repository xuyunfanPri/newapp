//
//  PGIndexNormalBanner.h
//  PRJ_LZNEWS
//
//  Created by launching on 2017/8/2.
//  Copyright © 2017年 launching. All rights reserved.
//

#import "PGIndexBannerSubiew.h"

@interface PGIndexNormalBanner : PGIndexBannerSubiew

@property (nonatomic, strong, readonly) UILabel *titleLabel;
@property (nonatomic, strong, readonly) CAGradientLayer *gradientLayer;

@end
