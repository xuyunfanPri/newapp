//
//  PGIndexNormalBanner.m
//  PRJ_LZNEWS
//
//  Created by launching on 2017/8/2.
//  Copyright © 2017年 launching. All rights reserved.
//

#import "PGIndexNormalBanner.h"

@interface PGIndexNormalBanner ()
@property (nonatomic, strong, readwrite) UILabel *titleLabel;
@property (nonatomic, strong, readwrite) CAGradientLayer *gradientLayer;
@end

@implementation PGIndexNormalBanner

- (id)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self.layer setMasksToBounds:YES];
        [self.layer setCornerRadius:10];
        [self.layer addSublayer:self.gradientLayer];
        [self addSubview:self.titleLabel];
    }
    return self;
}

- (void)setSubviewsWithSuperViewBounds:(CGRect)superViewBounds {
    
    if (CGRectEqualToRect(self.mainImageView.frame, superViewBounds)) {
        return;
    }
    
    self.mainImageView.frame = superViewBounds;
    self.coverView.frame = self.bounds;
    self.titleLabel.frame = CGRectMake(10.0, superViewBounds.size.height - 50.0, superViewBounds.size.width - 20, 46.0);
    self.gradientLayer.frame = CGRectMake(0.0, superViewBounds.size.height - 60.0, superViewBounds.size.width, 60.0);
}

- (UILabel *)titleLabel {
    if (!_titleLabel) {
        _titleLabel = [UILabel new];
        _titleLabel.backgroundColor = [UIColor clearColor];
        _titleLabel.textAlignment = NSTextAlignmentCenter;
        _titleLabel.textColor = [UIColor whiteColor];
        _titleLabel.font = [UIFont systemFontOfSize:14.0];
//        _titleLabel.textContainerInset = UIEdgeInsetsMake(0.0, kLeftMargin, 0.0, kRightMargin);
    }
    return _titleLabel;
}

- (CAGradientLayer *)gradientLayer {
    if (!_gradientLayer) {
        _gradientLayer = [CAGradientLayer new];
        UIColor *startColor = [UIColor clearColor];
        UIColor *endColor = [UIColor colorWithWhite:0 alpha:1];
        _gradientLayer.colors = @[(__bridge id)startColor.CGColor, (__bridge id)endColor.CGColor];
    }
    return _gradientLayer;
}

@end
