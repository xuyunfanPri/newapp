//
//  CalDataSorce.h
//  NewApp
//
//  Created by Alan on 2019/4/29.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface CalDataSorce : NSObject
- (NSInteger)convertDateToDay:(NSDate *)date;
- (NSInteger)convertDateToMonth:(NSDate *)date ;
- (NSInteger)convertDateToYear:(NSDate *)date ;
//根据date获取当月周几 (美国时间周日-周六为 1-7,改为0-6方便计算)
- (NSInteger)convertDateToWeekDay:(NSDate *)date ;
//根据date获取当月周几
- (NSInteger)convertDateToFirstWeekDay:(NSDate *)date ;
//根据date获取当月总天数
- (NSInteger)convertDateToTotalDays:(NSDate *)date ;
    //根据date获取偏移指定天数的date
- (NSDate *)getDateFrom:(NSDate *)date offsetDays:(NSInteger)offsetDays ;
//根据date获取偏移指定月数的date
- (NSDate *)getDateFrom:(NSDate *)date offsetMonths:(NSInteger)offsetMonths ;
//根据date获取偏移指定年数的date
- (NSDate *)getDateFrom:(NSDate *)date offsetYears:(NSInteger)offsetYears ;
- (void)test ;

@end


