//
//  XYNavigationController.h
//  NewApp
//
//  Created by Alan on 2019/4/11.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XYNavigationController : UINavigationController

@end

NS_ASSUME_NONNULL_END
