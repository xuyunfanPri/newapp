//
//  UIScreen+XYLibrary.m
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//

#import "UIScreen+XYLibrary.h"

@implementation UIScreen (XYLibrary)

+ (CGFloat)screenWidth {
    CGSize screenSize = [[self class] screenSize];
    return screenSize.width;
}

+ (CGFloat)screenHeight {
    CGSize screenSize = [[self class] screenSize];
    return screenSize.height;
}

+ (CGFloat)pixelThatFits:(CGFloat)pixel {
    CGFloat screenWidth = [UIScreen screenWidth];
    CGFloat pixelAspectRatio = screenWidth / kiPhone6Pixel;
    return floorf(pixelAspectRatio * pixel);
}

+ (CGSize)screenSize {
    CGRect bounds = [UIScreen mainScreen].bounds;
    UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
    if (UIInterfaceOrientationIsLandscape(orientation)) {
        CGFloat buffer = bounds.size.width;
        bounds.size.width = bounds.size.height;
        bounds.size.height = buffer;
    }
    return bounds.size;
}

@end
