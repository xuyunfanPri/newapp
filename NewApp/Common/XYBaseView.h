//
//  XYBaseView.h
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYBaseCommonHeader.h"
#import "Masonry.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XYBaseView : UIView

- (void)configureUIWithObj:(id)obj;
- (UIImage *)yp_imagecutWithOriginalImageView:(UIView *)superView withCutRect:(CGRect)rect;
+ (CGFloat)getViewHeightWithData:(id)obj;

@end

NS_ASSUME_NONNULL_END
