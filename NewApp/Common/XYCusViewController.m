//
//  XYCusViewController.m
//  NewApp
//
//  Created by Alan on 2019/4/11.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "XYCusViewController.h"

@interface XYCusViewController ()

@end

@implementation XYCusViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    [self configureTopView];
    // Do any additional setup after loading the view.
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES];
    [super viewWillAppear:animated];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:NO];
    [super viewWillDisappear:animated];
}

/**
 设置自带导航栏
 */
- (void)configureTopView {
    
    UIView *headerView = [[UIView alloc]initWithFrame:CGRectMake(0, kStatusBarHeight, SCREEN_WIDTH, kNavBarHeight)];
    headerView.backgroundColor = [UIColor clearColor];
    headerView.tag = kNaviBarHeaderView;
    [self.view addSubview:headerView];

    UIButton *leftBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    leftBtn.tag = kNaviBarLeftButton;
    [leftBtn setImage:[UIImage imageNamed:@"btn_back.png"] forState:UIControlStateNormal];
    [leftBtn addTarget:self action:@selector(didTopLeftButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:leftBtn];
    
    UILabel *titleLab = [[UILabel alloc]initWithFrame:CGRectMake(CGRectGetMaxX(leftBtn.frame), 0, SCREEN_WIDTH - 88, 44)];
    titleLab.font = [UIFont systemFontOfSize:18.0f];
    titleLab.tag = kNaviBarTitleLab;
    titleLab.textColor = [UIColor whiteColor];
    [headerView addSubview:titleLab];
    
    UIButton *rightBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    rightBtn.tag = kNaviBarRightButton;
    rightBtn.hidden = YES;
    [rightBtn setImage:[UIImage imageNamed:@"btn_white_add.png"] forState:UIControlStateNormal];
    [rightBtn addTarget:self action:@selector(didTopLeftButtonTouched:) forControlEvents:UIControlEventTouchUpInside];
    [headerView addSubview:rightBtn];
    
}

/**
 设置背景图
 */
- (void)configureBackgroundView{
    UIImageView *bgimage = [[UIImageView alloc]initWithFrame:self.view.frame];
    bgimage.image = [UIImage imageNamed:@"bg"];
    [self.view addSubview:bgimage];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)didTopLeftButtonTouched:(id)sender {
    if (self.navigationController.topViewController == self) {
        [self.navigationController popViewControllerAnimated:YES];
    } else {
        [self dismissViewControllerAnimated:YES completion:nil];
    }
}

- (UIStatusBarStyle)preferredStatusBarStyle {
    return UIStatusBarStyleDefault;
    
}

@end
