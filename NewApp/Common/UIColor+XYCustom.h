//
//  UIColor+XYCustom.h
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UIColor (XYCustom)

+ (UIColor *)getHegColor:(NSString *)hexColor;
+ (UIColor *)getHegColor:(NSString*)hexColor WithAlpha:(CGFloat)alpha;

@end

NS_ASSUME_NONNULL_END
