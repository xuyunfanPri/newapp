//
//  XYBaseView.m
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//

#import "XYBaseView.h"

@implementation XYBaseView

- (void)configureUIWithObj:(id)obj {
    
}

+ (CGFloat)getViewHeightWithData:(id)obj {
    return 0.0f;
}

#pragma mark - yp_imagecutWithOriginalImage: withCutRect: 根据指定的范围剪切图片中的一部分
/** originalImage：原图片   rect：需要剪切的位置*/
- (UIImage *)yp_imagecutWithOriginalImageView:(UIView *)superView withCutRect:(CGRect)rect {

    UIGraphicsBeginImageContext(superView.frame.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    [superView.layer renderInContext:context];
    UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    CGImageRef ref = CGImageCreateWithImageInRect(img.CGImage, rect);
    UIImage *i = [UIImage imageWithCGImage:ref];
    CGImageRelease(ref);
    return i;
}

+ (UIImage *)subImageFromImage:(UIImage *)image origin:(CGPoint)location size:(CGSize)subSize{
    
    CGFloat screenScale = [UIScreen mainScreen].scale;
    
    CGImageRef imageRef = CGImageCreateWithImageInRect([image CGImage], CGRectMake(location.x, location.y, subSize.width * screenScale, subSize.height * screenScale));
    UIImage *subImage = [UIImage imageWithCGImage:imageRef];
    CGImageRelease(imageRef);
    return subImage;
}

@end
