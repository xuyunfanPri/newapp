//
//  XYBaseCommonHeader.h
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//

#ifndef XYBaseCommonHeader_h
#define XYBaseCommonHeader_h

/// 获取像素适配(以iPhone6为标准尺寸)
#define PIXEL_FITS(pixel) [UIScreen pixelThatFits:pixel]

#import "UIScreen+XYLibrary.h"
#import "UIColor+XYCustom.h"

#endif /* XYBaseCommonHeader_h */
