//
//  UIScreen+XYLibrary.h
//  NewApp
//
//  Created by Qhtu on 2019/4/29.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import <objc/runtime.h>
#import "sys/utsname.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

static CGFloat const kiPhone6Pixel = 375.0;

@interface UIScreen (XYLibrary)

/**
 获取屏幕宽和高
 */
+ (CGFloat)screenWidth;
+ (CGFloat)screenHeight;

/**
 屏幕宽高适配(iPhone6为标准尺寸)
 */
+ (CGFloat)pixelThatFits:(CGFloat)pixel;

@end

NS_ASSUME_NONNULL_END
