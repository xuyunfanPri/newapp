//
//  XYCusViewController.h
//  NewApp
//
//  Created by Alan on 2019/4/11.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//
#import "XYBaseCommonHeader.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

#define kIs_iphone (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPhone)
#define kIs_iPhoneX SCREEN_WIDTH >=375.0f && SCREEN_HEIGHT >=812.0f&& kIs_iphone

/*状态栏高度*/
#define kStatusBarHeight (CGFloat)(kIs_iPhoneX?(44.0):(20.0))
/*导航栏高度*/
#define kNavBarHeight (44)
/*状态栏和导航栏总高度*/
#define kNavBarAndStatusBarHeight (CGFloat)(kIs_iPhoneX?(88.0):(64.0))
/*TabBar高度*/
#define kTabBarHeight (CGFloat)(kIs_iPhoneX?(49.0 + 34.0):(49.0))
/*顶部安全区域远离高度*/
#define kTopBarSafeHeight (CGFloat)(kIs_iPhoneX?(44.0):(0))
/*底部安全区域远离高度*/
#define kBottomSafeHeight (CGFloat)(kIs_iPhoneX?(34.0):(0))
/*iPhoneX的状态栏高度差值*/
#define kTopBarDifHeight (CGFloat)(kIs_iPhoneX?(24.0):(0))
/*导航条和Tabbar总高度*/
#define kNavAndTabHeight (kNavBarAndStatusBarHeight + kTabBarHeight)

#define kNaviBarHeaderView          90000
#define kNaviBarLeftButton          90001
#define kNaviBarRightButton         90002
#define kNaviBarBgImage             90003
#define kNaviBarTitleLab            90004


@interface XYCusViewController : UIViewController

- (void)configureTopView;
- (void)configureBackgroundView;

@end

NS_ASSUME_NONNULL_END
