//
//  DashEXLVC.m
//  NewApp
//
//  Created by Alan on 2019/4/18.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "DashEXLVC.h"
#import "Masonry.h"
@interface DashEXLVC ()

@end

@implementation DashEXLVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCircleView];
    [self setSubViews];
}
-(void)setCircleView{
    
    CircleView *cirView = [[CircleView alloc]initWithFrame:CGRectMake(20, 160, SCREEN_WIDTH-40, SCREEN_WIDTH - 40)];
    [cirView setProgress:0.5];
    
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 160, SCREEN_WIDTH, SCREEN_WIDTH)];
    [bgView addSubview:cirView];
    bgView.backgroundColor = [UIColor grayColor];
    [self.view addSubview:bgView];
//    [view mas_makeConstraints:^(MASConstraintMaker *make) {
//        make.centerY.equalTo(self.view.mas_centerY).offset(-20);
//        make.centerX.equalTo(self.view);
//        make.width.equalTo(@(SCREEN_WIDTH-40));
//        make.height.equalTo(@(SCREEN_WIDTH-40));
//    }];
    UIView *subView = [[UIView alloc]init];
    [self.view addSubview:subView];
    float width = SCREEN_WIDTH/3 - 30;
    float height = width/5;
    [subView mas_makeConstraints:^(MASConstraintMaker *make) {
        make.left.right.equalTo(self.view);
        make.bottom.equalTo(self.view).offset(-50);
        make.height.equalTo(@(height));
    }];
    UIView *rectView = [self subViewWithColour:[UIColor yellowColor] andconArr:@[@"163",@"fhksjd",@"werwwe"]];
    UIView *rectView2 = [self subViewWithColour:[UIColor yellowColor] andconArr:@[@"163",@"fhksjd",@"werwwe"]];
    UIView *rectView3 = [self subViewWithColour:[UIColor yellowColor] andconArr:@[@"163",@"fhksjd",@"werwwe"]];
    NSArray *viewArr = @[rectView,rectView2,rectView3];
    for (int i = 0; i<3; i++) {
        UIView *conview = viewArr[i];
        [subView addSubview:conview];
        [conview mas_makeConstraints:^(MASConstraintMaker *make) {
            make.top.equalTo(subView);
            make.left.equalTo(subView).offset(i*40+20);
            make.width.equalTo(@(width));
            make.height.equalTo(@(height));
        }];
        
    }
    
}
-(void)setSubViews{
    
}
-(UIView*)subViewWithColour:(UIColor*)colour andconArr:(NSArray *)arr{
    float width = SCREEN_WIDTH/3 - 40;
    float height = width/5;
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, width, width)];
    bgView.layer.borderColor = [colour CGColor];
    bgView.layer.borderWidth = 1;
    UILabel *numLab = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, width, width/2)];
    [bgView addSubview:numLab];
    
    UILabel *line1Lab = [[UILabel alloc]initWithFrame:CGRectMake(0, width/2, width, height)];
    UILabel *line2Lab = [[UILabel alloc]initWithFrame:CGRectMake(0, width/2+height, width, height)];
    [bgView addSubview:line1Lab];
    [bgView addSubview:line2Lab];
    line1Lab.textAlignment = NSTextAlignmentCenter;
    line2Lab.textAlignment = NSTextAlignmentCenter;
    
    numLab.textColor = colour;
    numLab.text = arr[0];
    line1Lab.text = arr[1];
    line2Lab.text = arr[2];
    
    
    return bgView;
}

// 返回列表视图
// 如果列表是VC，就返回VC.view
// 如果列表是View，就返回View自己
- (UIView *)listView {
    return self.view;
}

//可选使用，列表显示的时候调用
- (void)listDidAppear {}

//可选使用，列表消失的时候调用
- (void)listDidDisappear {}
@end
