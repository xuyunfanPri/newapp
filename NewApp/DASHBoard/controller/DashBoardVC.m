//
//  DashBoardVC.m
//  NewApp
//
//  Created by Alan on 2019/4/11.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "DashBoardVC.h"
#import "JXCategoryView.h"
#import "CircleView.h"
#import "SecondViewController.h"
@interface DashBoardVC ()<JXCategoryViewDelegate,JXCategoryListContainerViewDelegate>
@property (nonatomic,strong)JXCategoryTitleView *categoryView;
@property (nonatomic,strong)JXCategoryListContainerView *listContainerView;
@end

@implementation DashBoardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.navigationController.title = @"DASHBOARD";
    [self configureBackgroundView];
    [self layoutCateView];
    
}

-(void)layoutCateView{
    //初始化
    self.categoryView = [[JXCategoryTitleView alloc]initWithFrame:CGRectMake(0, 30, SCREEN_WIDTH, 50)];
    self.categoryView.delegate = self;
    self.categoryView.backgroundColor = [UIColor blackColor];
    self.categoryView.titleColor = [UIColor grayColor];
    self.categoryView.titleSelectedColor = [UIColor whiteColor];
    //配置属性
    self.categoryView.titles = @[@"EXL", @"ELH", @"SLE",@"ROP",@"QSY"];
    self.categoryView.titleColorGradientEnabled = YES;
    //添加指示器
    JXCategoryIndicatorLineView *lineView = [[JXCategoryIndicatorLineView alloc] init];
    lineView.indicatorLineViewColor = UIColorFromRGB(0x008be4);
    lineView.indicatorLineWidth = JXCategoryViewAutomaticDimension;
    self.categoryView.indicators = @[lineView];
    
    //初始化JXCategoryListContainerView
    self.listContainerView = [[JXCategoryListContainerView alloc] initWithDelegate:self];
    self.listContainerView.frame = CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT);
    [self.view addSubview:self.listContainerView];
    [self.view addSubview:self.categoryView];

    //关联cotentScrollView，关联之后才可以互相联动！！！
    self.categoryView.contentScrollView = self.listContainerView.scrollView;
    
}
//MARK:--categateView-Delegate
//点击选中或者滚动选中都会调用该方法。适用于只关心选中事件，不关心具体是点击还是滚动选中的。
- (void)categoryView:(JXCategoryBaseView *)categoryView didSelectedItemAtIndex:(NSInteger)index{
    [self.listContainerView didClickSelectedItemAtIndex:index];

    NSLog(@"%d",index);
}
    
    

//点击选中的情况才会调用该方法
- (void)categoryView:(JXCategoryBaseView *)categoryView didClickSelectedItemAtIndex:(NSInteger)index{
    
}

//滚动选中的情况才会调用该方法
- (void)categoryView:(JXCategoryBaseView *)categoryView didScrollSelectedItemAtIndex:(NSInteger)index{
    
}

//正在滚动中的回调
- (void)categoryView:(JXCategoryBaseView *)categoryView scrollingFromLeftIndex:(NSInteger)leftIndex toRightIndex:(NSInteger)rightIndex ratio:(CGFloat)ratio{
    [self.listContainerView scrollingFromLeftIndex:leftIndex toRightIndex:rightIndex ratio:ratio selectedIndex:categoryView.selectedIndex];

}

//自定义contentScrollView点击选中切换效果
- (void)categoryView:(JXCategoryBaseView *)categoryView didClickedItemContentScrollViewTransitionToIndex:(NSInteger)index{
    
}
//MARK:--JXCategoryListContainerViewDelegate代理方法
//返回列表的数量
- (NSInteger)numberOfListsInlistContainerView:(JXCategoryListContainerView *)listContainerView {
    return 5;
}
//返回遵从`JXCategoryListContentViewDelegate`协议的实例
- (id<JXCategoryListContentViewDelegate>)listContainerView:(JXCategoryListContainerView *)listContainerView initListForIndex:(NSInteger)index {
    CircleView *view = [[CircleView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, SCREEN_HEIGHT)];
    [view setProgress:0.5];
    return view;
    //return [[SecondViewController alloc]init];
}

@end
