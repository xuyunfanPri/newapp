//
//  CircleView.h
//  NewApp
//
//  Created by Alan on 2019/4/11.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXCategoryListContainerView.h"



NS_ASSUME_NONNULL_BEGIN

@interface CircleView : UIView<JXCategoryListContentViewDelegate>
/** 进度条的宽度 默认为10 */
@property (assign, nonatomic) CGFloat lineWidth;
/** 进度条的背景颜色 默认为灰色 */
@property (strong, nonatomic) UIColor *trackBackgroundColor;
/** 进度条的颜色 默认为蓝色 */
@property (strong, nonatomic) UIColor *trackColor;
/** 设置或者获取当前进度 */
@property (assign, nonatomic) CGFloat progress;
/** 进度条两端的样式 默认为kCGLineCapRound*/
@property (assign, nonatomic) CGLineCap lineCap;
/**  向下偏移*/
@property (assign,nonatomic) CGFloat topMar;

- (void)setProgress:(CGFloat)progress ;

@end

NS_ASSUME_NONNULL_END
