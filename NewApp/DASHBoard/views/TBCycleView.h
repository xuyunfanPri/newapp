//
//  TBCycleView.h
//  TBCycleProgress
//
//  Created by qianjianeng on 16/2/22.
//  Copyright © 2016年 SF. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXCategoryListContainerView.h"

@interface TBCycleView : UIView<JXCategoryListContentViewDelegate>

@property (nonatomic, strong) UILabel *label;
@property (assign)float radio;
@property (assign)float lineWidth;
- (void)drawProgress:(CGFloat )progress;

@end
