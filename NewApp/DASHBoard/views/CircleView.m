//
//  CircleView.m
//  NewApp
//
//  Created by Alan on 2019/4/11.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "CircleView.h"

@implementation CircleView


// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (instancetype)init {
    if (self = [super init]) {
        [self commonInit];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
    }
    return self;
}
- (void)drawRect:(CGRect)rect {
    CGFloat slideLength = self.bounds.size.width;
    [self custDrawtipAndPathWithRadio:(slideLength/2-_lineWidth*3)/3 andperCent:0.5 andcolour:[UIColor yellowColor]];
    [self custDrawtipAndPathWithRadio:(slideLength/2-_lineWidth*3)*2/3 andperCent:0.6 andcolour:[UIColor greenColor]];
    [self custDrawtipAndPathWithRadio:(slideLength/2-_lineWidth*3) andperCent:0.7 andcolour:[UIColor blueColor]];
}
-(void)custDrawtipAndPathWithRadio:(CGFloat)radio andperCent:(CGFloat)percent andcolour:(UIColor*)colour{
    //画圆环
    [self custDrawWithRadio:radio andperCent:percent andColour:colour];
    //画头上的点
    [self setHeaderImageWithRadio:radio andperCent:percent];
}
- (void)custDrawWithRadio:(CGFloat)radio andperCent:(CGFloat)percent andColour:(UIColor*)colour{
    //获取当前上下文
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGFloat slideLength = self.bounds.size.width;
    CGFloat centerX = slideLength/2;
    CGFloat centerY = slideLength/2+self.topMar;
    CGFloat startA = -M_PI/2;
    CGFloat endA = -M_PI/2 + 2*M_PI;
    // 添加背景轨道
    CGContextAddArc(currentContext, centerX, centerY, radio, startA, endA, 0);
    // 设置轨道宽度
    CGContextSetLineWidth(currentContext, _lineWidth);
    // 设置背景颜色
    [[UIColor blackColor] setStroke];
    // 绘制轨道
    CGContextStrokePath(currentContext);
    
    // 进度条轨道
    CGFloat deltaAngle = percent*2*M_PI - M_PI/2;
    // 根据进度progress的值绘制进度条
    // 注意: 角度需要使用弧度制
    // 设置圆心x, y坐标
    // 设置圆的半径 -- 这里需要的自然是(边长-轨道宽度)/2
    // 从beginAngle 绘制到endAngle= beginAngle+deltaAngle;
    CGContextAddArc(currentContext, centerX, centerY, radio, startA, deltaAngle, 0);
    // 设置进度条颜色
    [colour setStroke];
    //
    CGContextSetLineWidth(currentContext, _lineWidth);
    // 设置轨道端点的样式
    CGContextSetLineCap(currentContext, _lineCap);
    // 使用stroke方式填充路径
    CGContextStrokePath(currentContext);
    
    
}
//MARK:--画顶端的圆
-(void)setHeaderImageWithRadio:(CGFloat)radio andperCent:(CGFloat)percent{
    UIImageView *imageView = [[UIImageView alloc]init];
    imageView.image = [self drawImage];
    CGFloat angle = percent*2*M_PI - M_PI/2;
    CGFloat slideLength = self.bounds.size.width;
    CGFloat centerX = slideLength/2;
    CGFloat centerY = slideLength/2+self.topMar;
    CGFloat r = radio;
    CGFloat imageCenterX = centerX + r*cos(angle);
    CGFloat imageCenterY = centerY + r*sin(angle);
    // 这里 + 8 作为imageView比轨道宽8的效果
    imageView.frame = CGRectMake(0, 0, _lineWidth, _lineWidth);
    imageView.center = CGPointMake(imageCenterX, imageCenterY);
    [self addSubview:imageView];
}
- (UIImage *)drawImage {
    UIGraphicsBeginImageContext(CGSizeMake(20, 20));
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
    CGContextAddArc(currentContext, 10, 10, 10, 0, 2*M_PI, 0);
    [[UIColor whiteColor] set];
    CGContextFillPath(currentContext);
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return image;
}
- (void)commonInit {
    _trackBackgroundColor = [UIColor blackColor];
    _trackColor = [UIColor blueColor];
    _lineWidth = 15;
    _lineCap = kCGLineCapRound;
    _topMar = 120;
    //_beginAngle = radiusFromAngle(-90);
    //    self.clipsToBounds = NO;
    //    self.layer.masksToBounds = YES;
    self.backgroundColor = [UIColor clearColor];
    //[self addSubview:self.progressLabel];
}

- (void)setProgress:(CGFloat)progress {
    
    if (progress > 1 || progress < 0) return;
    _progress = progress;
   
    [self setNeedsDisplay];
   
}
// 返回列表视图
// 如果列表是VC，就返回VC.view
// 如果列表是View，就返回View自己
- (UIView *)listView {
    return self;
}

//可选使用，列表显示的时候调用
- (void)listDidAppear {}

//可选使用，列表消失的时候调用
- (void)listDidDisappear {}
@end
