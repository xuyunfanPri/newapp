//
//  TBCycleView.m
//  TBCycleProgress
//
//  Created by qianjianeng on 16/2/22.
//  Copyright © 2016年 SF. All rights reserved.
//

#import "TBCycleView.h"
@interface TBCycleView ()

@property (nonatomic, assign) CGFloat progress;

@property (nonatomic, strong) CAShapeLayer *progressLayer;

@property (nonatomic, strong) CALayer *gradientLayer;
@property (nonatomic,strong)UIImageView *imageView;

@end
@implementation TBCycleView

- (UILabel *)label
{
    if (_label == nil) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        label.textAlignment = NSTextAlignmentCenter;
        label.center = CGPointMake(self.frame.size.width / 2, self.frame.size.height / 2);
        [self addSubview:label];
        label.text = @"test";
        label.font = [UIFont fontWithName:FONTNAMEREG size:30];
        label.textColor = UIColorFromRGB(0xe5ce10);
        _label = label;
    }
    return _label;
}
- (instancetype)init {
    if (self = [super init]) {
        [self commonInit];
        [self addSubview:self.label];
    }
    return self;
}

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        [self commonInit];
        [self addSubview:self.label];

    }
    return self;
}
- (instancetype)initWithCoder:(NSCoder *)aDecoder {
    if (self = [super initWithCoder:aDecoder]) {
        [self commonInit];
        [self addSubview:self.label];

    }
    return self;
}
-(void)commonInit{
    self.backgroundColor = [UIColor clearColor];
    self.radio = 100;
    self.lineWidth = 20;
}
- (void)drawProgress:(CGFloat )progress
{
    _progress = progress;
    self.label.text = [NSString stringWithFormat:@"%.0f",progress*100];
    [_progressLayer removeFromSuperlayer];
    [_gradientLayer removeFromSuperlayer];
    [self setNeedsDisplay];
}

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    [self drawCycleProgress];
    //[self drawImage];
    
}
-(void)layoutSubviews{
    [super layoutSubviews];
    [self addSubview:[self label]];
    CGFloat angle = _progress*2*M_PI - M_PI_2;
    CGFloat centerX = self.bounds.size.width/2;
    CGFloat centerY = self.bounds.size.width/2;
    CGFloat r = self.radio;
    CGFloat imageCenterX = centerX + r*cos(angle);
    CGFloat imageCenterY = centerY + r*sin(angle);
        // 这里 + 8 作为imageView比轨道宽8的效果
    self.imageView.frame = CGRectMake(0, 0, self.lineWidth+8, self.lineWidth+8);
    self.imageView.center = CGPointMake(imageCenterX, imageCenterY);
    [self addSubview:self.imageView];
}
-(void)drawImage{
    CGFloat angle = _progress*2*M_PI - M_PI_2;
    CGFloat centerX = self.bounds.size.width/2;
    CGFloat centerY = self.bounds.size.width/2;
    CGFloat r = 100;
    CGFloat imageCenterX = centerX + r*cos(angle);
    CGFloat imageCenterY = centerY + r*sin(angle);
    // 这里 + 8 作为imageView比轨道宽8的效果
    self.imageView.backgroundColor = [UIColor whiteColor];
    self.imageView.frame = CGRectMake(0, 0, 20+8, 20+8);
    self.imageView.center = CGPointMake(imageCenterX, imageCenterY);
}
- (void)drawCycleProgress
{
    CGFloat centerX = self.bounds.size.width/2;
    CGFloat centerY = self.bounds.size.width/2;
    CGPoint center = CGPointMake(centerX, centerY);
    CGFloat radius = 100;
    CGFloat startA = - M_PI_2;  //设置进度条起点位置
    CGFloat endA = -M_PI_2 + M_PI * 2 * _progress;  //设置进度条终点位置
    
    
    //获取环形路径（画一个圆形，填充色透明，设置线框宽度为10，这样就获得了一个环形）
    _progressLayer = [CAShapeLayer layer];//创建一个track shape layer
    _progressLayer.frame = self.bounds;
    _progressLayer.fillColor = [[UIColor clearColor] CGColor];  //填充色为无色
    _progressLayer.strokeColor = [[UIColor redColor] CGColor]; //指定path的渲染颜色,这里可以设置任意不透明颜色
    _progressLayer.opacity = 1; //背景颜色的透明度
    _progressLayer.lineCap = kCALineCapRound;//指定线的边缘是圆的
    _progressLayer.lineWidth = 20;//线的宽度
    UIBezierPath *path = [UIBezierPath bezierPathWithArcCenter:center radius:radius startAngle:startA endAngle:endA clockwise:YES];//上面说明过了用来构建圆形
    _progressLayer.path =[path CGPath]; //把path传递給layer，然后layer会处理相应的渲染，整个逻辑和CoreGraph是一致的。
    [self.layer addSublayer:_progressLayer];
    //生成渐变色
    _gradientLayer = [CALayer layer];
    
    //左侧渐变色
    CAGradientLayer *leftLayer = [CAGradientLayer layer];
    leftLayer.frame = CGRectMake(0, 0, self.bounds.size.width / 2, self.bounds.size.height);    // 分段设置渐变色
    leftLayer.locations = @[@0.01, @0.5, @1];
    UIColor *colour1 = [UIColor colorWithRed:226/255.0 green:61/255.0 blue:150/255.0 alpha:1];
    UIColor *color2 = [UIColor colorWithRed:251/255.0 green:175/255.0 blue:52/255.0 alpha:1];
    //UIColor *color3 = [UIColor colorWithRed:235/255.0 green:118/255.0 blue:101/255.0 alpha:1];
    UIColor *color4 = [UIColor colorWithRed:191/255.0 green:215/255.0 blue:48/255.0 alpha:1];

    leftLayer.colors = @[(id)colour1.CGColor, (id)color2.CGColor,(id)color4.CGColor];
    [_gradientLayer addSublayer:leftLayer];
    
    //右侧渐变色
    CAGradientLayer *rightLayer = [CAGradientLayer layer];
    rightLayer.frame = CGRectMake(self.bounds.size.width / 2, 0, self.bounds.size.width / 2, self.bounds.size.height);
    leftLayer.locations = @[@0.05, @0.5, @1];
    
    rightLayer.colors = @[(id)colour1.CGColor, (id)color2.CGColor,(id)color4.CGColor];

    [_gradientLayer addSublayer:rightLayer];
    
    [self.layer setMask:_progressLayer]; //用progressLayer来截取渐变层
    [self.layer addSublayer:_gradientLayer];
}

- (UIImageView *)imageView {
    if (!_imageView) {
        UIImageView *imageView = [[UIImageView alloc] init];
        imageView.layer.masksToBounds = YES;
        imageView.clipsToBounds = YES;
        imageView.layer.cornerRadius = 9;
        imageView.backgroundColor = [UIColor brownColor];
        _imageView = imageView;
    }
    
    return _imageView;
}
// 返回列表视图
// 如果列表是VC，就返回VC.view
// 如果列表是View，就返回View自己
- (UIView *)listView {
    return self;
}

//可选使用，列表显示的时候调用
- (void)listDidAppear {}

//可选使用，列表消失的时候调用
- (void)listDidDisappear {}
@end
