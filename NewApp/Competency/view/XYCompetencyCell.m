//
//  XYCompetencyCell.m
//  NewApp
//
//  Created by Qhtu on 2019/5/9.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//

#import "XYCompetencyCell.h"

@implementation XYCompetencyCell

- (void)loadCellWithData:(id)obj {
    
}

+ (CGFloat)getCellHeightWithData:(id)obj {
    return 0.0f;
}

@end

@interface XYCompetencyHomeCardCell()

@end

@implementation XYCompetencyHomeCardCell

- (instancetype)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
    if (self = [super initWithStyle:style reuseIdentifier:reuseIdentifier]) {
        XYCompetencyHomeCardView *cardView = [[XYCompetencyHomeCardView alloc]init];
        [self.contentView addSubview:cardView];
        self.cardView = cardView;
        self.contentView.backgroundColor = [UIColor clearColor];
        self.backgroundColor = [UIColor clearColor];
        CGFloat leftMargin = (SCREEN_WIDTH - (kProgressCardWidth + 2 * kProgressLeftMargin)) / 2.0f;
        [cardView setFrame:CGRectMake(leftMargin, 0, kProgressCardWidth + 2 * kProgressLeftMargin, [[self class] getCellHeightWithData:nil])];
    }
    return self;
}

- (void)loadCellWithData:(id)obj {
    [self.cardView configureUIWithObj:obj];
}

+ (CGFloat)getCellHeightWithData:(id)obj {
    
    return [XYCompetencyHomeCardView getViewHeightWithData:obj];
}

@end
