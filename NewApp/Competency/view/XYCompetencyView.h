//
//  XYCompetencyView.h
//  NewApp
//
//  Created by Qhtu on 2019/5/5.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//

#import "XYBaseView.h"

#define kProgressCardWidth          PIXEL_FITS(252)

#define kProgressTopMargin          10
#define kProgressTitleMargin        12.5
#define kProgressLeftMargin         PIXEL_FITS(20)
#define kProgressHeight             20

NS_ASSUME_NONNULL_BEGIN

//首页卡片进度条
@interface XYCompetencyProgressView : XYBaseView

@property (nonatomic, readonly) UILabel *titleLab;
@property (nonatomic, readonly) UIImageView *grayProgressImg;       //灰色进度条
@property (nonatomic, readonly) UIImageView *progressImg;           //彩色进度条

- (void)changeViewWithProgress:(CGFloat)progressRadio;

@end

//首页卡片
@interface XYCompetencyHomeCardView : XYBaseView

@property (nonatomic, readonly) UILabel *titleLab;
@property (nonatomic, readonly) XYCompetencyProgressView *progressView1;
@property (nonatomic, readonly) XYCompetencyProgressView *progressView2;
@property (nonatomic, readonly) XYCompetencyProgressView *progressView3;
@property (nonatomic, readonly) XYCompetencyProgressView *progressView4;
@property (nonatomic, readonly) XYCompetencyProgressView *progressView5;

@end

NS_ASSUME_NONNULL_END
