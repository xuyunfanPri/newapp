//
//  XYCompetencyView.m
//  NewApp
//
//  Created by Qhtu on 2019/5/5.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//

#import "XYCompetencyView.h"

@interface XYCompetencyProgressView()

@property (nonatomic, readwrite) UILabel *titleLab;
@property (nonatomic, readwrite) UILabel *progressLab;               //进度数值
@property (nonatomic, readwrite) UIImageView *grayProgressImg;       //灰色进度条
@property (nonatomic, readwrite) UIImageView *progressImg;           //彩色进度条

@end

@implementation XYCompetencyProgressView

- (instancetype)initWithFrame:(CGRect)frame {
    if (self = [super initWithFrame:frame]) {
        self.backgroundColor = [UIColor clearColor];
        self.titleLab = [[UILabel alloc]initWithFrame:CGRectMake(0, kProgressTopMargin, frame.size.width - PIXEL_FITS(60), PIXEL_FITS(13))];
        self.titleLab.textColor = [UIColor getHegColor:@"ffffff" WithAlpha:0.6];
        self.titleLab.font = [UIFont systemFontOfSize:13];
        self.titleLab.backgroundColor = [UIColor clearColor];
        [self addSubview:self.titleLab];
        
        self.progressLab = [[UILabel alloc]initWithFrame:CGRectMake(frame.size.width - PIXEL_FITS(50), kProgressTopMargin, PIXEL_FITS(50), PIXEL_FITS(13))];
        self.progressLab.textColor = [UIColor getHegColor:@"e5ce10" WithAlpha:1];
        self.progressLab.font = [UIFont systemFontOfSize:13];
        self.progressLab.backgroundColor = [UIColor clearColor];
        [self addSubview:self.progressLab];
        
        self.grayProgressImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, kProgressTopMargin + PIXEL_FITS(13) + 12.5, kProgressCardWidth, kProgressHeight)];
        self.grayProgressImg.image = [UIImage imageNamed:@"progress_gray"];
        [self addSubview:self.grayProgressImg];
        
        self.progressImg = [[UIImageView alloc]initWithFrame:CGRectMake(0, kProgressTopMargin + PIXEL_FITS(13) + kProgressTitleMargin, 0, kProgressHeight)];
        self.progressImg.image = [UIImage imageNamed:@"progress_colors"];
        self.progressImg.contentMode = UIViewContentModeLeft;
        self.progressImg.backgroundColor = [UIColor clearColor];
        [self addSubview:self.progressImg];
        
    }
    return self;
}

- (void)configureUIWithObj:(id)obj {
    self.titleLab.text = @"Rate Of progress 01";
    self.progressLab.text = @"60%";
    //设置进度条百分比
    [self changeViewWithProgress:0.6];
}

+ (CGFloat)getViewHeightWithData:(id)obj {
    
    CGSize titleSize = [@"" sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:13.0f]}];
    
    return kProgressTopMargin + titleSize.height + kProgressTitleMargin + kProgressHeight + kProgressTopMargin;
}

- (void)changeViewWithProgress:(CGFloat)progressRadio {
    CGFloat progressWidth = self.frame.size.width - kProgressLeftMargin * 2;
    CGFloat leftProgressWidth = progressWidth * progressRadio;
    [self.progressImg setFrame:CGRectMake(0, kProgressTopMargin + PIXEL_FITS(13) + kProgressTitleMargin,leftProgressWidth, kProgressHeight)];
    
    UIImage *newImage = [self yp_imagecutWithOriginalImageView:self.progressImg withCutRect:CGRectMake(0, 0, leftProgressWidth, kProgressHeight)];
    self.progressImg.image = newImage;
}

@end

@interface XYCompetencyHomeCardView()

@property (nonatomic, readwrite) UILabel *titleLab;
@property (nonatomic, readwrite) XYCompetencyProgressView *progressView1;
@property (nonatomic, readwrite) XYCompetencyProgressView *progressView2;
@property (nonatomic, readwrite) XYCompetencyProgressView *progressView3;
@property (nonatomic, readwrite) XYCompetencyProgressView *progressView4;
@property (nonatomic, readwrite) XYCompetencyProgressView *progressView5;

@property (nonatomic, strong) NSMutableArray *progressViewArr;

@end

@implementation XYCompetencyHomeCardView

- (instancetype)init {
    if (self = [super init]) {
        self.backgroundColor = [UIColor grayColor];
        [self.layer setMasksToBounds:YES];
        [self.layer setCornerRadius:15];
        
        self.titleLab = [[UILabel alloc]initWithFrame:CGRectMake(kProgressLeftMargin, 20, kProgressCardWidth, PIXEL_FITS(16))];
        self.titleLab.textColor = [UIColor whiteColor];
        self.titleLab.text = @"Grade01";
        self.titleLab.font = [UIFont systemFontOfSize:16.0f];
        [self addSubview:self.titleLab];
        
        CGFloat progressHeight = [XYCompetencyProgressView getViewHeightWithData:nil];
        
        self.progressView1 = [[XYCompetencyProgressView alloc]initWithFrame:CGRectMake(kProgressLeftMargin, PIXEL_FITS(16) + 30, kProgressCardWidth, progressHeight)];
        self.progressView1.titleLab.text = @"Rate Of progress 01";
        [self addSubview:self.progressView1];
        
        self.progressView2 = [[XYCompetencyProgressView alloc]initWithFrame:CGRectMake(kProgressLeftMargin, CGRectGetMaxY(self.progressView1.frame), kProgressCardWidth, progressHeight)];
        self.progressView2.titleLab.text = @"Rate Of progress 02";
        [self addSubview:self.progressView2];
        
        self.progressView3 = [[XYCompetencyProgressView alloc]initWithFrame:CGRectMake(kProgressLeftMargin, CGRectGetMaxY(self.progressView2.frame), kProgressCardWidth, progressHeight)];
        self.progressView3.titleLab.text = @"Rate Of progress 03";
        [self addSubview:self.progressView3];
        
        self.progressView4 = [[XYCompetencyProgressView alloc]initWithFrame:CGRectMake(kProgressLeftMargin, CGRectGetMaxY(self.progressView3.frame), kProgressCardWidth, progressHeight)];
        self.progressView4.titleLab.text = @"Rate Of progress 04";
        [self addSubview:self.progressView4];
        
        self.progressView5 = [[XYCompetencyProgressView alloc]initWithFrame:CGRectMake(kProgressLeftMargin, CGRectGetMaxY(self.progressView4.frame), kProgressCardWidth, progressHeight)];
        self.progressView5.titleLab.text = @"Rate Of progress 05";
        [self addSubview:self.progressView5];
        
        self.progressViewArr = [[NSMutableArray alloc]init];
        [self.progressViewArr addObject:self.progressView1];
        [self.progressViewArr addObject:self.progressView2];
        [self.progressViewArr addObject:self.progressView3];
        [self.progressViewArr addObject:self.progressView4];
        [self.progressViewArr addObject:self.progressView5];
    }
    return self;
}

- (void)configureUIWithObj:(id)obj {
    for (XYCompetencyProgressView *progressView in self.progressViewArr) {
        [progressView configureUIWithObj:nil];
    }
}

+ (CGFloat)getViewHeightWithData:(id)obj {
    CGSize titleSize = [@"" sizeWithAttributes:@{NSFontAttributeName:[UIFont systemFontOfSize:16.0f]}];
    
    CGFloat progressHeight = [XYCompetencyProgressView getViewHeightWithData:nil];
    
    return 20 + titleSize.height + 30 + progressHeight * 5;

}

@end
