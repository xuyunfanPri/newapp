//
//  XYCompetencyCell.h
//  NewApp
//
//  Created by Qhtu on 2019/5/9.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYCompetencyView.h"
#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface XYCompetencyCell : UITableViewCell

- (void)loadCellWithData:(id)obj;

+ (CGFloat)getCellHeightWithData:(id)obj;

@end

@interface XYCompetencyHomeCardCell : XYCompetencyCell

@property (nonatomic, strong) XYCompetencyHomeCardView *cardView;

@end

NS_ASSUME_NONNULL_END
