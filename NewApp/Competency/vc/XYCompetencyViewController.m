//
//  XYCompetencyViewController.m
//  NewApp
//
//  Created by Qhtu on 2019/5/5.
//  Copyright © 2019年 Xuyunfan. All rights reserved.
//
#import "XYCompetencyCell.h"
#import "XYCompetencyViewController.h"

@interface XYCompetencyViewController () <UITableViewDelegate,UITableViewDataSource>

@property (nonatomic, strong) UITableView *cardTableView;
@property (nonatomic, strong) UIScrollView *scrollView;

@end

@implementation XYCompetencyViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureTopView];
    
    [self configureTableView];
    // Do any additional setup after loading the view.
}

- (void)configureTopView {
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];
    
    UIButton *rightBtn = [headerView viewWithTag:kNaviBarRightButton];
    rightBtn.hidden = NO;
}

- (void)configureTableView {
    UIView *headerView = [self.view viewWithTag:kNaviBarHeaderView];

    self.cardTableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 0,[XYCompetencyHomeCardCell getCellHeightWithData:nil],SCREEN_WIDTH) style:UITableViewStylePlain];

    CGFloat topMargin = 90;
    self.cardTableView.center = CGPointMake(self.view.frame.size.width / 2, (self.view.frame.size.height / 2) + topMargin);

    self.cardTableView.dataSource = self;
    self.cardTableView.delegate = self;
    self.cardTableView.backgroundColor = [UIColor clearColor];
    self.cardTableView.transform = CGAffineTransformMakeRotation(-M_PI / 2);
    self.cardTableView.center = CGPointMake(self.view.frame.size.width / 2, (self.view.frame.size.height / 2) - CGRectGetMaxY(headerView.frame));
    self.cardTableView.showsVerticalScrollIndicator = NO;
    self.cardTableView.backgroundColor = [UIColor clearColor];
    self.cardTableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    [self.view addSubview:self.cardTableView];
}

#pragma mark - UITableViewDelegate

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 5;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *identifierStr = @"XYCompetencyHomeCardCell";
    XYCompetencyHomeCardCell *cell = [tableView dequeueReusableCellWithIdentifier:identifierStr];
    if (!cell) {
        cell = [[XYCompetencyHomeCardCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:identifierStr];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        // cell顺时针旋转90度
        cell.contentView.transform = CGAffineTransformMakeRotation(M_PI / 2);

    }
    
    [cell loadCellWithData:nil];
    
    return cell;
}



- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return [XYCompetencyHomeCardCell getCellHeightWithData:nil];
}

@end
