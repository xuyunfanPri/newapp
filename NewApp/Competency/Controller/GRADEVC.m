//
//  GRADEVC.m
//  NewApp
//
//  Created by Alan on 2019/5/10.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "GRADEVC.h"
#import "JXCategoryView.h"

@interface GRADEVC ()
@property (nonatomic,strong)JXCategoryTitleView *categoryView;
@property (nonatomic,strong)JXCategoryListContainerView *listContainerView;
@end

@implementation GRADEVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self configureBackgroundView];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
