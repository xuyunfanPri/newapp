//
//  AppraisalBaseVC.m
//  NewApp
//
//  Created by Alan on 2019/4/21.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "AppraisalBaseVC.h"
#import "ProcessView.h"
@interface AppraisalBaseVC ()<UITableViewDelegate,UITableViewDataSource>
@end

@implementation AppraisalBaseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = [UIColor blackColor];
    
    self.view.frame = CGRectMake(0, 20, SCREEN_WIDTH, SCREEN_HEIGHT);
    UIImageView *bgimage = [[UIImageView alloc]initWithFrame:self.view.frame];
    bgimage.image = [UIImage imageNamed:@"bg"];
    [self.view addSubview:bgimage];
    [self layoutHeaderView];
    [self layoutTableview];
    
    
}

-(void)layoutHeaderView{
    UIView *headerbgView = [[UIView alloc]initWithFrame:CGRectMake(0, 20, SCREEN_WIDTH, 44)];
    headerbgView.backgroundColor = [UIColor clearColor];
    [self.view addSubview:headerbgView];
    UIButton *btn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, 44, 44)];
    [btn setImage:[UIImage imageNamed:@"btn_back"] forState:UIControlStateNormal];
    [btn addTarget:self action:@selector(back) forControlEvents:UIControlEventTouchUpInside];
    
    [headerbgView addSubview:btn];
    
    UIButton *btn2 = [[UIButton alloc]initWithFrame:CGRectMake(SCREEN_WIDTH-44, 0, 44, 44)];
    [btn2 setImage:[UIImage imageNamed:@"btn_home"] forState:UIControlStateNormal];
    [btn2 addTarget:self action:@selector(home) forControlEvents:UIControlEventTouchUpInside];
    [headerbgView addSubview:btn2];
}
-(void)back{
    　[self dismissViewControllerAnimated:NO completion:nil];
}
-(void)home{
    
    UIViewController *parentVC = self.presentingViewController;
    UIViewController *bottomVC;
    while (parentVC) {
        bottomVC = parentVC;
        parentVC = parentVC.presentingViewController;
    }
    [bottomVC dismissViewControllerAnimated:NO completion:^{
        //dismiss后再切换根视图
       // [UIApplication sharedApplication].delegate.window.rootViewController = [TabBarController new];
    }];
    return;
}
-(void)layoutTableview{
    UITableView *tableView = [[UITableView alloc]initWithFrame:CGRectMake(0, 64, SCREEN_WIDTH, SCREEN_HEIGHT-64)];
//    tableView.delegate = self;
//    tableView.dataSource = self;
    tableView.backgroundColor = [UIColor clearColor];
    tableView.separatorStyle = UITableViewCellSeparatorStyleNone;
    self.tableView = tableView;
    [self.view addSubview:tableView];
}

//MARK:--tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 1;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    return 80;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    
    
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ProcessView *pView = [[ProcessView alloc]initWithStep:1];
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}



-(UITableViewCell *)headerCellWithstep:(int)step{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ProcessView *pView = [[ProcessView alloc]initWithStep:step];
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}


@end
