//
//  AppraisalStepOneVC.m
//  NewApp
//
//  Created by Alan on 2019/4/22.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "AppraisalStepOneVC.h"
#import "InfoFormVC.h"
#import "ProcessView.h"
#import "AppraisalStepTwoVC.h"
@interface AppraisalStepOneVC ()<UITableViewDelegate,UITableViewDataSource,btmViewDelegate>

@end

@implementation AppraisalStepOneVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}

//MARK:--tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return 6;
    
}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.row == 0) {
        return 80;
    }
    if (indexPath.row == 1) {
        return 140;
    }
    if (indexPath.row == 2) {
        return 140;
    }
    if (indexPath.row == 3) {
        return 260;
    }
    if (indexPath.row == 4) {
        return 200;
    }
    if (indexPath.row == 5) {
        return 70;
    }
    return 80;
    
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if (indexPath.row == 0) {
        return [self headerCellWithstep:1];
    }
    if (indexPath.row >= 1 && indexPath.row <=4) {
        return [self formCellWithtag:indexPath.row];
    }
    if (indexPath.row == 5) {
        return [self buttomCellWithstep:indexPath.row];
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

-(UITableViewCell*)formCellWithtag:(int)tag{
    UITableViewCell *cell = [[UITableViewCell alloc]init];
    InfoFormVC *info = [[InfoFormVC alloc]initWithStep:[self tfArrWithTag:tag]];
    [cell addSubview:info];
    cell.backgroundColor = [UIColor clearColor];
    return cell;
}



-(NSArray *)tfArrWithTag:(int)tag{
    if (tag == 1) {
        UITextField*tf1 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In Employee..."];
        UITextField*tf2 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In Employee..."];
        return @[tf1,tf2];
    }
    if (tag == 2) {
        UITextField*tf1 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In Manager..."];
        UITextField*tf2 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In One-above Manager..."];
        return @[tf1,tf2];
    }
    if (tag == 3) {
        UITextField*tf1 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In Unit..."];
        UITextField*tf2 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In Location..."];
        UITextField*tf3 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In Position Title..."];
        UITextField*tf4 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In Role..."];

        return @[tf1,tf2,tf3,tf4];
    }
    if (tag == 4) {
        UITextField*tf1 = [InfoFormVC textFieldWithPlacehold:@"Please Fill In Review Period..."];
        UITextField*tf2 = [InfoFormVC textFieldWithPlacehold:@"Please Select Creation Date"];
        UITextField*tf3 = [InfoFormVC textFieldWithPlacehold:@"Please Select Review Date"];
        return @[tf1,tf2,tf3];
    }
    return nil;
}
-(UITableViewCell *)buttomCellWithstep:(int)step{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ButtomView *pView = [[ButtomView alloc]initWithStep:1];
    pView.delegate = self;
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}
-(void)backOrNextBtnClick:(int)tag{
    NSLog(@"%d",tag);
    if (tag == 1) {
        [self presentViewController:[[AppraisalStepTwoVC alloc]init] animated:NO completion:nil];
    }
}
@end
