//
//  AppraisalChooseVC.m
//  NewApp
//
//  Created by Alan on 2019/4/24.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "AppraisalChooseVC.h"
#import "ProcessView.h"
#import "ChooseViewContent.h"
#import "QuestionModel.h"
#import "AppraisalCommentVC.h"
@interface AppraisalChooseVC ()<UITableViewDelegate,UITableViewDataSource,btmViewDelegate>

@property (nonatomic,strong)QuestionModel *model;

@end

@implementation AppraisalChooseVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    
}

//MARK:--tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return 1;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 80;
    }
    if (indexPath.section == 1) {
        return [self.model caluSectionHeight];
    }
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return [self headerCellWithstep:3];
    }
    if (indexPath.section == 1) {
        return [self sectionViewWithTag:indexPath.section];
    }
    if (indexPath.section == 2) {
        return [self buttomCellWithstep:3];
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    
    return cell;
}
-(UITableViewCell *)sectionViewWithTag:(int)tag{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ChooseViewContent *pView = [[ChooseViewContent alloc]initWithModel:self.model];
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}
-(UITableViewCell *)headerCellWithstep:(int)step{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ProcessView *pView = [[ProcessView alloc]initWithStep:step];
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}
-(QuestionModel *)model{
    if (!_model) {
        _model = [self createDate];
    }
    return _model;
}
-(QuestionModel*)createDate{
    NSMutableDictionary *dic = [NSMutableDictionary dictionary];
    dic[@"title"] = @"ENERGY FOR RENEWAL";
    dic[@"ansArr"] = @[@"REFYSES TO ADOPT NEW WAYS TO WORK,REFYSES TO ADOPT NEW WAYS TO WORK,",@"REFYSES TO ADOPT NEW WAYS TO WORK",@"REFYSES TO ADOPT NEW WAYS TO WORK",@"REFYSES TO ADOPT NEW WAYS TO WORK"];
    QuestionModel *model = [[QuestionModel alloc]initWithdic:dic];
    return model;
}
-(UITableViewCell *)buttomCellWithstep:(int)step{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ButtomView *pView = [[ButtomView alloc]initWithStep:1];
    pView.delegate = self;
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    return cell;
}
-(void)backOrNextBtnClick:(int)tag{
    NSLog(@"%d",tag);
    if (tag == 1) {
        [self presentViewController:[[AppraisalCommentVC alloc]init] animated:NO completion:nil];
    }
}
@end
