//
//  AppraisalStepTwoVC.m
//  NewApp
//
//  Created by Alan on 2019/4/23.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "AppraisalStepTwoVC.h"
#import "ProcessView.h"
#import "DocumentView.h"
#import "DocModel.h"
#import "AppraisalChooseVC.h"
@interface AppraisalStepTwoVC ()<UITableViewDelegate,UITableViewDataSource,btmViewDelegate>

@end

@implementation AppraisalStepTwoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
}
//MARK:--tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 2;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 80;
    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        return 50;
    }
    return [self culaHeight:indexPath.section];
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return [self headerCellWithstep:2];
    }
    if (indexPath.section == 0 && indexPath.row == 1) {
        return [self headertext];
    }
    
    if (indexPath.section == 1) {
        return [self pdfAndDocView];
    }
    if (indexPath.section == 2) {
        return [self pdfAndDocView];
    }
    if (indexPath.section == 3) {
        return [self buttomCellWithstep:2];
    }
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    
    return cell;
}
-(UITableViewCell *)headertext{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    cell.backgroundColor = [UIColor clearColor];
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(0, 0,SCREEN_WIDTH, 50)];
    label.text = @"SAFETY";
    label.textAlignment = NSTextAlignmentCenter;
    label.font = [UIFont fontWithName:FONTNAMEREG size:12];
    label.font = [UIFont systemFontOfSize:12];
    label.textColor = UIColorFromAlphaRGB(0xffffff, 0.4);
    label.backgroundColor = [UIColor clearColor];
    [cell addSubview:label];
    return cell;
}
-(float)culaHeight:(long)section{
    float bigen = 20+40+20;

    float viewHeight =(4/3 + 1)*(80+20) + 20 + 40+15+10+20;

    if (section == 1|| section == 2) {
        return viewHeight;
    }
    return 40;
}
-(UITableViewCell *)pdfAndDocView{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    NSMutableArray *arr = [NSMutableArray array];
    for (int i = 0; i<4; i++) {
        [arr addObject:[DocModel new]];
    }
    DocumentView *view = [[DocumentView alloc]initWithHeader:@"2X PASSED SAFETY DOCUMENT FROM SUPERVISOR(UPVISE PDF)" anddocs:arr];
    cell.backgroundColor =[ UIColor clearColor];
    [cell addSubview:view];
    
    return cell;
}
-(UITableViewCell *)headerCellWithstep:(int)step{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ProcessView *pView = [[ProcessView alloc]initWithStep:step];
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}
-(UITableViewCell *)buttomCellWithstep:(int)step{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ButtomView *pView = [[ButtomView alloc]initWithStep:1];
    pView.delegate = self;
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}



-(void)backOrNextBtnClick:(int)tag{
    NSLog(@"%d",tag);
    if (tag == 1) {
        [self presentViewController:[[AppraisalChooseVC alloc]init] animated:NO completion:nil];
    }
}

@end
