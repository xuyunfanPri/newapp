//
//  AppraisalCommentVC.m
//  NewApp
//
//  Created by Alan on 2019/4/25.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "AppraisalCommentVC.h"
#import "ProcessView.h"
#import "SelectView.h"
#import "AppraisalChartVC.h"
@interface AppraisalCommentVC ()<UITableViewDelegate,UITableViewDataSource,UITextViewDelegate,btmViewDelegate>
@property (nonatomic,strong)UITextView *textView;
@end

@implementation AppraisalCommentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    self.tableView.delegate = self;
    self.tableView.dataSource = self;
    // Do any additional setup after loading the view.
}

//MARK:--tableview
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    return 4;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    if (section == 0) {
        return 1;
    }
    if (section == 1) {
        return 1;
    }
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return 80;
    }
    if (indexPath.section == 1) {
        return 80;
    }
    if (indexPath.section == 2) {
        float height = SCREEN_HEIGHT-160-50-64;
        return height +5;
    }
    return 50;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    if (indexPath.section == 0 && indexPath.row == 0) {
        return [self headerCellWithstep:4];
    }
    if (indexPath.section == 1) {
        return [self sectionChooseView];
    }
    if (indexPath.section == 2) {
        return [self commentView];
    }
    if (indexPath.section == 3) {
        return [self buttomCellWithstep:4];
    }
    
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    
    return cell;
}
-(UITableViewCell *)sectionChooseView{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    SelectView *pView = [[SelectView alloc]initWithString:@"Please Select Supervisor" andtype:0];
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}
-(UITableViewCell *)commentView{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    float height = SCREEN_HEIGHT-160-50-64;
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(20, 10, SCREEN_WIDTH-40, height)];
    bgView.backgroundColor = UIColorFromAlphaRGB(0xffffff, 0.06);
    bgView.layer.cornerRadius = 3;
    [bgView addSubview:self.textView];
    
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:bgView];
    
    return cell;
}
-(UITableViewCell *)headerCellWithstep:(int)step{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ProcessView *pView = [[ProcessView alloc]initWithStep:step];
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}
-(UITableViewCell *)buttomCellWithstep:(int)step{
    UITableViewCell *cell = [[UITableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"Cell"];
    ButtomView *pView = [[ButtomView alloc]initWithStep:1];
    pView.delegate = self;
    cell.backgroundColor = [UIColor clearColor];
    [cell addSubview:pView];
    
    return cell;
}
-(void)backOrNextBtnClick:(int)tag{
    NSLog(@"%d",tag);
    if (tag == 1) {
        [self presentViewController:[[AppraisalChartVC alloc]init] animated:NO completion:nil];
    }
}
- (UITextView *)textView {
    
    if (!_textView) {
        float height = SCREEN_HEIGHT-160-50-64;

        _textView = [[UITextView alloc]initWithFrame:CGRectMake(20, 20, SCREEN_HEIGHT-80, height-40)];

//        _textView = [[UITextView alloc] init];
        _textView.backgroundColor = [UIColor clearColor];
        _textView.delegate = self;
        _textView.text = @"Please Fill In Comments...";
        // 设置PlaceHolder的字体颜色
        _textView.textColor = UIColorFromAlphaRGB(0xffffff, 0.8);
        //  设置字体（里面的宏和常量是我项目中自己定义的）
        _textView.font = [UIFont fontWithName:FONTNAMEREG size:12];
    }
    return _textView;
}

- (void)textViewDidBeginEditing:(UITextView *)textView {
    // 在已经开始编辑的方法中判断如果当前textView.text为PlaceHolder的值 就将其值改变为空字符串
    if ([textView.text isEqualToString:@"Please Fill In Comments..."]) {
        textView.font = [UIFont fontWithName:FONTNAME size:12];

        textView.text = @"";
    }
    //textView.textColor = [UIColor xk_colorWithHexString:kBTitleColor];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
    // 在结束编辑的时候去判断 如果没有输入文字  那么重新将PlaceHolder设置上，同时将text的颜色修改回来
    if ([textView.text isEqualToString:@""]) {
        
        textView.text = @"Please Fill In Comments...";
        
        textView.textColor = UIColorFromAlphaRGB(0xffffff, 0.8);
        textView.font = [UIFont fontWithName:FONTNAME size:12];
    }
}

@end
