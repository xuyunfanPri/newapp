//
//  AppraisalBaseVC.h
//  NewApp
//
//  Created by Alan on 2019/4/21.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "XYCusViewController.h"

NS_ASSUME_NONNULL_BEGIN

@interface AppraisalBaseVC : XYCusViewController
@property (nonatomic,strong)UITableView *tableView;


-(UITableViewCell *)headerCellWithstep:(int)step;

@end

NS_ASSUME_NONNULL_END
