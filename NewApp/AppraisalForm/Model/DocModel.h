//
//  DocModel.h
//  NewApp
//
//  Created by Alan on 2019/4/23.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface DocModel : NSObject
@property (nonatomic,strong)NSString *type;
@end

NS_ASSUME_NONNULL_END
