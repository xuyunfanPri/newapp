//
//  QuestionModel.h
//  NewApp
//
//  Created by Alan on 2019/4/24.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface QuestionModel : NSObject
@property (assign)int chooseType;


//content
@property (nonatomic,strong)NSString *title;
@property (nonatomic,strong)NSArray *ansArr;


//rect
@property (assign)float textWidth;
@property (nonatomic,strong) NSMutableArray *textHeightArr;

-(float)caluSectionHeight;
- (instancetype)initWithdic:(NSDictionary*)dic;


@property (assign) int answer;

@end

NS_ASSUME_NONNULL_END
