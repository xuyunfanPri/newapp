//
//  QuestionModel.m
//  NewApp
//
//  Created by Alan on 2019/4/24.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//NSMutableParagraphStyle.h

#import "QuestionModel.h"
#import "ViewUtils.h"

@implementation QuestionModel

- (instancetype)initWithdic:(NSDictionary*)dic
{
    self = [super init];
    if (self) {
        _title = dic[@"title"];
        _ansArr = dic[@"ansArr"];
        _textWidth = SCREEN_WIDTH-40-35-21-12-20;
        [self caluHeights];
    }
    return self;
}

-(NSMutableArray *)textHeightArr{
    if (!_textHeightArr) {
        _textHeightArr = [NSMutableArray array];
        
    }
    return _textHeightArr;
}

-(void)caluHeights{
    for (NSString *str in self.ansArr) {
        float rowLeight = [ViewUtils getSpaceLabelHeightwithContentString:str andFont:10 andViewWidth:_textWidth-20];
        [self.textHeightArr addObject:@(rowLeight+5)];
    }
}

-(float)caluSectionHeight{
    float viewHeight = 20 + 20 + 20;
    for (NSNumber *num in self.textHeightArr) {
        viewHeight = viewHeight + [num floatValue] + 5 + 20;
    }
    return viewHeight-20-20;
}



@end
