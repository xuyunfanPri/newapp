//
//  DocumentView.m
//  NewApp
//
//  Created by Alan on 2019/4/23.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "DocumentView.h"
#import "DocModel.h"
@implementation DocumentView

-(instancetype)initWithHeader:(NSString *)string anddocs:(NSArray*)docs{
    float viewHeight = (int)(docs.count/3 + 1)*(80+20) + 20 + 40+15+10;
    self = [super initWithFrame:CGRectMake(20, 10, SCREEN_WIDTH-40, viewHeight)];
    if (self) {
        self.backgroundColor = UIColorFromAlphaRGB(0xffffff, 0.06);
        [self addtitleView:string];
        [self addDocsView:docs];
    }
    return self;
}
-(void)addtitleView:(NSString*)title{
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 25, 5, 15)];
    [self addSubview:lineView];
    lineView.image = [UIImage imageNamed:@"line"];
    UILabel *leftLab = [[UILabel alloc]init];
    leftLab.text = title;
    leftLab.font = [UIFont fontWithName:FONTNAMEREG size:12];
    leftLab.font = [UIFont systemFontOfSize:12];
    leftLab.frame = CGRectMake(35, 20, SCREEN_WIDTH-35-20, 40);
    leftLab.numberOfLines = 2;
    leftLab.textColor = [UIColor whiteColor];
    leftLab.backgroundColor = [UIColor clearColor];
    [self addSubview:leftLab];
}
-(void)addDocsView:(NSArray*)docs{
    float bigen = 20+40+20;
    float width = (SCREEN_WIDTH-20-20-15-40-40)/3;

    for (int i = 0; i < docs.count; i++) {
        UIView *btnView = [self docView:docs[i]];
        int row = i/3;
        int col = i%3;
        btnView.frame = CGRectMake(35 + col*(width+20), bigen + row*(80+20), width, 80) ;
        [self addSubview:btnView];
    }
}
-(UIView *)docView:(DocModel*)model{
    float width = (SCREEN_WIDTH-20-20-15-40)/3;
    UIButton *docBtn = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, width, 80)];
    docBtn.backgroundColor = UIColorFromAlphaRGB(0xffffff, 0.1);
    docBtn.layer.cornerRadius = 3;
    UIButton *detBtn = [[UIButton alloc]initWithFrame:CGRectMake(-10, -10, 20, 20)];
    [detBtn addTarget:self action:@selector(detBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    [detBtn setImage:[UIImage imageNamed:@"btn_del2"] forState:UIControlStateNormal];
    detBtn.backgroundColor = [UIColor clearColor];
    [docBtn addSubview:detBtn];
    
    float Btnwidth = (SCREEN_WIDTH-20-20-15-60)/3;

    UIImageView *docImage = [[UIImageView alloc]initWithFrame:CGRectMake(Btnwidth/2-15-2.5, 10, 30, 30)];
    docImage.image = [UIImage imageNamed:@"icon_DOC"];
    [docBtn addSubview:docImage];
    
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(5, 45, width-20, 35)];
    label.text = @"PASSED SAFETY DOCUME...";
    label.numberOfLines = 2;
    label.font = [UIFont fontWithName:FONTNAME size:7.5];
    label.font = [UIFont systemFontOfSize:7.5];
    label.textColor = UIColorFromAlphaRGB(0xffffff, 0.4);
    [docBtn addSubview:label];
    return docBtn;
}


-(void)detBtnClick:(UIButton *)btn{
    
}

@end
