//
//  InfoFormVC.m
//  NewApp
//
//  Created by Alan on 2019/4/22.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "InfoFormVC.h"
#import "Masonry.h"

@implementation InfoFormVC
- (instancetype)initWithStep:(NSArray*)viewArr
{
    self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 60*viewArr.count+20)];
    self.backgroundColor = [UIColor clearColor];
    if (self) {
        [self layoutCustomViewWithlabelView:viewArr];
    }
    return self;
}
-(void)layoutCustomViewWithlabelView:(NSArray*)arr{
    UIView *bgView = [[UIView alloc]initWithFrame:CGRectMake(20, 10, SCREEN_WIDTH-40, 60*arr.count)];
    bgView.layer.cornerRadius = 3;
    bgView.layer.masksToBounds = YES;
    bgView.backgroundColor = UIColorFromAlphaRGB(0xffffff, 0.06);
    [self addSubview:bgView];
    [self layoutLabels:arr andBgView:bgView];
}
-(void)layoutLabels:(NSArray *)arr andBgView:(UIView*)bgView{
    for (int i = 0; i<arr.count; i++) {
        UITextField *label = arr[i];
        [bgView addSubview:label];
        label.frame = CGRectMake(20, 60*i, SCREEN_WIDTH-80, 59);
        
        if (i<arr.count - 1) {
            UIView *lineView = [[UIView alloc]initWithFrame:CGRectMake(20, 60*(i+1)-1, SCREEN_WIDTH-80, 1)];
            lineView.backgroundColor = UIColorFromAlphaRGB(0xffffff, 0.8);
            [bgView addSubview:lineView];
        }
    }
}
+(UITextField*)textFieldWithPlacehold:(NSString*)placehold{
    UITextField *tf = [[UITextField alloc]init];
    tf.placeholder = placehold;
    tf.font = [UIFont fontWithName:FONTNAMEREG size:12];
    return tf;
}
    
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
