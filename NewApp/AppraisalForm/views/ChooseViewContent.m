//
//  ChooseViewContent.m
//  NewApp
//
//  Created by Alan on 2019/4/24.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "ChooseViewContent.h"
#import "QuestionModel.h"
#import "RadioButton.h"



@implementation ChooseViewContent

- (instancetype)initWithModel:(QuestionModel*)model
{
    self = [super initWithFrame:CGRectMake(20, 10, SCREEN_WIDTH-40, [model caluSectionHeight])];

    if (self) {
        //self.backgroundColor = [UIColor blueColor];
        self.backgroundColor = UIColorFromAlphaRGB(0xffffff, 0.06);
        self.model = model;
        [self addtitleView:model.title];
        [self addDocsView:model];
    }
    return self;
}
-(void)addtitleView:(NSString*)title{
    UIImageView *lineView = [[UIImageView alloc]initWithFrame:CGRectMake(20, 25, 5, 15)];
    [self addSubview:lineView];
    lineView.image = [UIImage imageNamed:@"line"];
    UITextView *leftLab = [[UITextView alloc]init];
    leftLab.text = title;
    leftLab.font = [UIFont fontWithName:FONTNAMEREG size:12];
    leftLab.font = [UIFont systemFontOfSize:12];
    leftLab.frame = CGRectMake(35, 15, SCREEN_WIDTH-35-20, 45);
    leftLab.textColor = [UIColor whiteColor];
    leftLab.backgroundColor = [UIColor clearColor];
    leftLab.userInteractionEnabled = NO;
    [self addSubview:leftLab];
}

-(void)addDocsView:(QuestionModel*)model{
    if (model.chooseType == 0) {
        //danxuan
        float startHeight = 20 + 20 + 20;
        [RadioButton addObserverForGroupId:@"op1" observer:self];
        for (int i = 0; i<model.ansArr.count; i++) {
            RadioButton *lineView = [[RadioButton alloc]initWithGroupId:@"op1" index:i];
            lineView.frame = CGRectMake(35+10, startHeight, 21, 21);
            [self addSubview:lineView];
            UITextView *leftLab = [[UITextView alloc]init];
            leftLab.text = model.ansArr[i];
//            NSMutableParagraphStyle *paragraphStyle = [[NSMutableParagraphStyle alloc] init];
//            paragraphStyle.lineSpacing = 5;// 字体的行间距
//
//            NSDictionary *attributes = @{
//                                         NSFontAttributeName:[UIFont fontWithName:FONTNAMEREG size:10],
//                                         NSParagraphStyleAttributeName:paragraphStyle
//                                         };
//            leftLab.font = [UIFont systemFontOfSize:10];
            //leftLab.attributedText = [[NSAttributedString alloc] initWithString:model.ansArr[i] attributes:attributes];

            leftLab.frame = CGRectMake(35+21+12, startHeight-10, model.textWidth, [model.textHeightArr[i] floatValue]);
            leftLab.textColor = UIColorFromAlphaRGB(0xffffff, 0.3);
            leftLab.backgroundColor = [UIColor clearColor];
            leftLab.userInteractionEnabled = NO;
            [self addSubview:leftLab];
            startHeight += [model.textHeightArr[i] floatValue] + 10;
        }
    }
}

-(void)radioButtonSelectedAtIndex:(NSUInteger)index inGroup:(NSString*)groupId{
    if ([groupId isEqualToString:@"op1"]) {
        self.model.answer = index;
    }
}



@end
