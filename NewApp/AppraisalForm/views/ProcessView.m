
//
//  ProcessView.m
//  NewApp
//
//  Created by Alan on 2019/4/21.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "ProcessView.h"
#import "Masonry.h"


@implementation ProcessView

- (instancetype)initWithStep:(int)step
{
    self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 80)];
    self.backgroundColor = [UIColor clearColor];
    if (self) {
        [self layoutCustomViewWithStep:step];
    }
    return self;
}
-(void)layoutCustomViewWithStep:(int)step{
    float rolWidth = (SCREEN_WIDTH - 50)/8;
    float start = 25+rolWidth+(step-1)*rolWidth*2;
    UIImageView *bgimageView = [[UIImageView alloc]initWithFrame:CGRectMake(25, 25, SCREEN_WIDTH-50, 10)];
    bgimageView.image = [UIImage imageNamed:@"loading"];
    bgimageView.layer.cornerRadius = 5;
    bgimageView.layer.masksToBounds = YES;
    [self addSubview:bgimageView];
    UIImageView *grayView = [[UIImageView alloc]initWithFrame:CGRectMake(start, 25, SCREEN_WIDTH-25-start, 10)];
    grayView.backgroundColor = [UIColor grayColor];
    grayView.layer.cornerRadius = 5;
    grayView.layer.masksToBounds = YES;
    [self addSubview:grayView];
    [self layoutNums:step];
    [self layoutLabels:step];
    
}
-(void)layoutLabels:(int)step{
    for (int i=1; i<5; i++) {
        if (i<=step) {
            [self subTextViewWithStep:i andselect:YES andtext:@"xxxxxx"];
        }else{
            [self subTextViewWithStep:i andselect:NO andtext:@"xxxxxx"];
        }
    }
}
-(void)layoutNums:(int)step{
    for (int i=1; i<5; i++) {
        if (i<=step) {
            [self shuziViewWithStep:i andselect:YES];
        }else{
            [self shuziViewWithStep:i andselect:NO];
        }
    }
}
-(void)shuziViewWithStep:(int)step andselect:(BOOL)select{
    float rolWidth = (SCREEN_WIDTH - 50)/8;
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake(15+rolWidth+(step-1)*rolWidth*2, 20, 20, 20)];
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds=YES;
    label.text = [NSString stringWithFormat:@"%d",step];
    label.textAlignment = NSTextAlignmentCenter;
    if (select) {
        label.backgroundColor = [UIColor whiteColor];
        label.textColor = UIColorFromRGB(0x2a2a2a);
        label.font = [UIFont systemFontOfSize:10.5];
    }else{
        label.font = [UIFont fontWithName:@"CourierNewPSMT" size:10.5];
        label.backgroundColor = [UIColor grayColor];
        label.textColor = UIColorFromRGB(0xffffff);
    }
    [self addSubview:label];
}
-(void)subTextViewWithStep:(int)step andselect:(BOOL)select andtext:(NSString*)text{
    float rolWidth = (SCREEN_WIDTH - 50)/8;
    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake((-5)+rolWidth+(step-1)*rolWidth*2, 55, 60, 20)];
    label.layer.cornerRadius = 10;
    label.layer.masksToBounds=YES;
    label.text = text;
    label.textAlignment = NSTextAlignmentCenter;
    if (select) {
        label.backgroundColor = [UIColor clearColor];
        label.textColor = UIColorFromRGB(0xffffff);
        label.font = [UIFont fontWithName:FONTNAME size:12];
    }else{
        label.font = [UIFont fontWithName:FONTNAME size:12];
        label.backgroundColor = [UIColor clearColor];
        
        label.textColor = UIColorFromAlphaRGB(0xffffff, 0.4);
    }
    [self addSubview:label];
}
@end

@implementation ButtomView

-(instancetype)initWithStep:(int)step{
    self = [super initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 70)];
    if (self) {
        [self lauoutSubView:step];
    }
    return self;
}
-(void)lauoutSubView:(int)step{
    float deHeight = 20;
    
    
    UILabel *leftLab = [[UILabel alloc]init];
    UIImageView *leftImage = [[UIImageView alloc]init];
    UILabel *rightlab = [[UILabel alloc]init];
    UIImageView *rightimage = [[UIImageView alloc]init];
    UIButton *leftBtn = [[UIButton alloc]init];
    leftBtn.frame = CGRectMake(20,10 , 150, 50);
    leftBtn.backgroundColor = [UIColor clearColor];
    [leftBtn addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    leftBtn.tag = 0;
    UIButton *rightBtn = [[UIButton alloc]init];
    rightBtn.frame = CGRectMake(SCREEN_WIDTH-150,10 , 150, 50);
    rightBtn.backgroundColor = [UIColor clearColor];
    [rightBtn addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    rightBtn.tag = 1;
    if (step == 1) {
        [self addSubview:rightlab];
        [self addSubview:rightimage];
        [self addSubview:rightBtn];

    }else if (step == 4){
        [self addSubview:leftLab];
        [self addSubview:leftImage];
        [self addSubview:leftBtn];

    }else{
        [self addSubview:leftLab];
        [self addSubview:leftImage];
        [self addSubview:rightlab];
        [self addSubview:rightimage];
        [self addSubview:leftBtn];
        [self addSubview:rightBtn];
    }
    leftImage.image = [UIImage imageNamed:@"btn_left"];
    leftImage.frame = CGRectMake(20, 20, deHeight, deHeight);
    
    leftLab.text = @"LAST STEP";
    leftLab.font = [UIFont fontWithName:FONTNAME size:14];
    leftLab.frame = CGRectMake(20+deHeight, 20, 150, deHeight);
    leftLab.textColor = [UIColor whiteColor];
    
    rightimage.image = [UIImage imageNamed:@"btn_right"];
    rightimage.frame = CGRectMake(SCREEN_WIDTH-20-deHeight, 20, deHeight, deHeight);
    rightlab.text = @"NEXT STEP";
    rightlab.font = [UIFont fontWithName:FONTNAME size:14];
    rightlab.frame = CGRectMake(SCREEN_WIDTH-20-deHeight-150, 20, 150, deHeight);
    rightlab.textAlignment = NSTextAlignmentRight;
    rightlab.textColor = [UIColor whiteColor];
    
}
-(void)leftBtnClick:(UIButton*)btn{
    if (self.delegate ) {
        [self.delegate backOrNextBtnClick:btn.tag];
    }
}
@end
