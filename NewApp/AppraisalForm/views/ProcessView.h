//
//  ProcessView.h
//  NewApp
//
//  Created by Alan on 2019/4/21.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol btmViewDelegate <NSObject>

-(void)backOrNextBtnClick:(int)tag;

@end

@interface ProcessView : UIView
- (instancetype)initWithStep:(int)step;

@end

@interface ButtomView : UIView
@property (nonatomic,weak)id<btmViewDelegate> delegate;
- (instancetype)initWithStep:(int)step;

@end

NS_ASSUME_NONNULL_END
