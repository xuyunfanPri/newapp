//
//  RatVC.m
//  NewApp
//
//  Created by Alan on 2019/4/26.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "RatVC.h"
#import "Masonry.h"
#import "TBCycleView.h"
#import "CalDataSorce.h"
@interface RatVC ()
@property (nonatomic,strong)UILabel *label;
@property (nonatomic,strong)UIImageView *imageView;
@end

@implementation RatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setCircleView];
    [self setSubViews];
    CalDataSorce *datr = [[CalDataSorce alloc]init];
    [datr test];
    
}
-(void)setCircleView{
    CGRect rect = CGRectMake(SCREEN_WIDTH/2-150, 170, 300, 300);
    UIView *bgView = [[UIView alloc]initWithFrame:rect];
    
    TBCycleView *view = [[TBCycleView alloc]initWithFrame:CGRectMake(0, 0, 300, 300)];
    [view drawProgress:0.7];
    [bgView addSubview:view];
    [self.view addSubview:bgView];
    self.label.text = @"2";
    [bgView addSubview:self.label];
    [bgView addSubview:[self setImage:0.7]];
}
-(UIImageView*)setImage:(float)progress{
    CGFloat angle = progress*2*M_PI - M_PI_2;
    CGFloat centerX = 150;
    CGFloat centerY = 150;
    CGFloat r = 100;
    CGFloat imageCenterX = centerX + r*cos(angle);
    CGFloat imageCenterY = centerY + r*sin(angle);
    // 这里 + 8 作为imageView比轨道宽8的效果
    self.imageView.backgroundColor = [UIColor whiteColor];
    self.imageView.frame = CGRectMake(0, 0, 20+8, 20+8);
    self.imageView.center = CGPointMake(imageCenterX, imageCenterY);
    return self.imageView;
}
- (UILabel *)label
{
    if (_label == nil) {
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, 100, 100)];
        label.textAlignment = NSTextAlignmentCenter;
        label.center = CGPointMake(150, 150);
        //[self addSubview:label];
        label.font = [UIFont fontWithName:FONTNAMEREG size:30];
        label.font = [UIFont systemFontOfSize:30];
        label.textColor = UIColorFromRGB(0xe5ce10);
        _label = label;
    }
    return _label;
}

-(void)setSubViews{
    
}

- (UIImageView *)imageView {
    if (!_imageView) {
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 28, 28)];
        imageView.layer.masksToBounds = YES;
        imageView.clipsToBounds = YES;
        imageView.layer.cornerRadius = 14;
        imageView.backgroundColor = [UIColor whiteColor];
        _imageView = imageView;
    }
    return _imageView;
}
// 返回列表视图
// 如果列表是VC，就返回VC.view
// 如果列表是View，就返回View自己
- (UIView *)listView {
    return self.view;
}

//可选使用，列表显示的时候调用
- (void)listDidAppear {}

//可选使用，列表消失的时候调用
- (void)listDidDisappear {}
@end
