//
//  RatVC.h
//  NewApp
//
//  Created by Alan on 2019/4/26.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXCategoryListContainerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface RatVC : UIViewController<JXCategoryListContentViewDelegate>

@end

NS_ASSUME_NONNULL_END
