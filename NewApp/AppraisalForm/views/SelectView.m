//
//  SelectView.m
//  NewApp
//
//  Created by Alan on 2019/4/25.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "SelectView.h"

@implementation SelectView
- (instancetype)initWithString:(NSString*)title andtype:(int)type
{
    self = [super initWithFrame:CGRectMake(20, 10, SCREEN_WIDTH-40, 60)];
    if (self) {
        self.backgroundColor = UIColorFromAlphaRGB(0xffffff, 0.06);

        [self layoutCustomView:title andtype:type];
    }
    return self;
}
         
-(void)layoutCustomView:(NSString*)title andtype:(int)type{
    UILabel *leftLab = [[UILabel alloc]init];
    UIImageView *leftImage = [[UIImageView alloc]init];
    UIButton *leftBtn = [[UIButton alloc]init];
    leftBtn.frame = CGRectMake(20,0, SCREEN_WIDTH - 40, 60);
    leftBtn.backgroundColor = [UIColor clearColor];
    [leftBtn addTarget:self action:@selector(leftBtnClick:) forControlEvents:UIControlEventTouchUpInside];
    
    [self addSubview:leftLab];
    [self addSubview:leftImage];
        [self addSubview:leftBtn];
    leftImage.image = [UIImage imageNamed:@"btn_pulldown"];
    leftImage.frame = CGRectMake(SCREEN_WIDTH - 50 - 30, 17.5, 25, 25);
    
    leftLab.text = title;
    leftLab.font = [UIFont fontWithName:FONTNAME size:12];
    leftLab.frame = CGRectMake(20,0, SCREEN_WIDTH - 190, 60);
    leftLab.textColor = UIColorFromAlphaRGB(0xffffff, 0.8);
    

}
-(void)leftBtnClick:(UIButton *)btn{
    
}
/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
