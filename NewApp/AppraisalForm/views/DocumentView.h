//
//  DocumentView.h
//  NewApp
//
//  Created by Alan on 2019/4/23.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DocumentView : UIView
-(instancetype)initWithHeader:(NSString *)string anddocs:(NSArray*)docs;

@end

NS_ASSUME_NONNULL_END
