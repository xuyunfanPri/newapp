//
//  SelectView.h
//  NewApp
//
//  Created by Alan on 2019/4/25.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SelectView : UIView
- (instancetype)initWithString:(NSString*)title andtype:(int)type;

@end

NS_ASSUME_NONNULL_END
