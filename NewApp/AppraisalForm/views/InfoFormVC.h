//
//  InfoFormVC.h
//  NewApp
//
//  Created by Alan on 2019/4/22.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InfoFormVC : UIView
- (instancetype)initWithStep:(NSArray*)viewArr;

+(UITextField*)textFieldWithPlacehold:(NSString*)placehold;
@end

NS_ASSUME_NONNULL_END
