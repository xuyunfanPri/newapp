//
//  ChooseViewContent.h
//  NewApp
//
//  Created by Alan on 2019/4/24.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "QuestionModel.h"
#import "RadioButton.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChooseViewContent : UIView<RadioButtonDelegate>
@property (nonatomic,strong)QuestionModel *model;
- (instancetype)initWithModel:(QuestionModel*)model;

@end

NS_ASSUME_NONNULL_END
