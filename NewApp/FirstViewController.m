//
//  FirstViewController.m
//  NewApp
//
//  Created by Alan on 2019/4/10.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import "FirstViewController.h"
#import "CircleView.h"
#import "JXCategoryView.h"
@interface FirstViewController ()
@property (nonatomic,strong)JXCategoryTitleView *categoryView;
@end

@implementation FirstViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    CircleView *view = [[CircleView alloc]initWithFrame:CGRectMake(0, 0, 200, 200)];
    [self.view addSubview:view];
    view.backgroundColor = [UIColor whiteColor];
    [view setProgress:0.5];
    [self layoutCateView];
}

-(void)layoutCateView{
    self.categoryView = [[JXCategoryTitleView alloc]initWithFrame:CGRectMake(0, 0, SCREEN_WIDTH, 50)];
}
@end
