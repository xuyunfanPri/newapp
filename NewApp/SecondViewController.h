//
//  SecondViewController.h
//  NewApp
//
//  Created by Alan on 2019/4/10.
//  Copyright © 2019 Xuyunfan. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "JXCategoryListContainerView.h"

@interface SecondViewController : UIViewController<JXCategoryListContentViewDelegate>


@end

